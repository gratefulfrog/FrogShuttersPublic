#include "ShutterActionMgr.h"
#include "App.h"

const conditionActionPair ShutterActionMgr::_caPairVec[_nbConditions] = 
      {{&ShutterActionMgr::_postPulseHoldCondition,    &ShutterActionMgr::_noAction},
       {&ShutterActionMgr::_postPulseUnHoldCondition,  &ShutterActionMgr::_unHoldAction},
       {&ShutterActionMgr::_unPulseCondition,          &ShutterActionMgr::_unPulseAction},
       {&ShutterActionMgr::_morePulseCondition,        &ShutterActionMgr::_noAction},
       {&ShutterActionMgr::_condition0,                &ShutterActionMgr::_safetyAction},
       {&ShutterActionMgr::_condition1,                &ShutterActionMgr::_resetSafetyAction},
       {&ShutterActionMgr::_condition2,                &ShutterActionMgr::_resetThresholdCrossingTimeAction},
       {&ShutterActionMgr::_condition3,                &ShutterActionMgr::_releaseAction}};

bool ShutterActionMgr::_postPulseHoldCondition(unsigned long now) const{
  // no actions for a specific amount of time
  static bool showMsg = true;
  bool res =(_noActionDelay  && (now -_yokisPulseStartTime < _yokisNoPulseDelay));
  if (res && showMsg){
    Serial.println("post pulse hold condtion");
    showMsg = false;
  }
  else if (!res){
    showMsg=true;
  }
  return res;
}
bool ShutterActionMgr::_postPulseUnHoldCondition(unsigned long now) const{
  // no actions for a specific amount of time
  
  bool res =(_noActionDelay  && (now -_yokisPulseStartTime >= _yokisNoPulseDelay));
  if (res ){
    Serial.println("post pulse unhold condtion");
  }
  return res;
}
bool ShutterActionMgr::_unPulseCondition(unsigned long now) const{
  // pulsing and time since start of puls is >= min pulse time
  bool res =(_pulsing && (now -_yokisPulseStartTime >= _yokisPulseTime));
  if (res){
    Serial.println("UnPulse condtion");
  }
  return res;
}
bool ShutterActionMgr::_morePulseCondition(unsigned long now) const{
  // pulsing and time since start of puls is >= min pulse time
  bool res  = (_pulsing && (now -_yokisPulseStartTime < _yokisPulseTime)); 
  return res;
}

bool ShutterActionMgr::_condition0(unsigned long now) const{
  // ws >= Safety Threshold AND Not SAFTEY
  bool res = ((_app.getCurrentWindSpeed() >= _app.getSafetyThresholdValue()) && 
              (_app.getShutterPosition() != App::shutterPostionSafety) &&
              (_app.getShutterPosition() != App::shutterPostionReleasing));
  if (res){
    Serial.println("condtion0");
  }
  return res;
}
bool ShutterActionMgr::_condition1(unsigned long now) const{
  // Release Threshold < ws  AND releasing
  float ws = _app.getCurrentWindSpeed(),
        rt = _app.getReleaseThresholdValue();
  byte pos = _app.getShutterPosition();
  bool res = (rt < ws) && (pos == App::shutterPostionReleasing);
  if (res){
    Serial.println("condtion1");
  }
  return res;
}
bool ShutterActionMgr::_condition2(unsigned long now) const{ 
  // ws <= Release Threshold AND Safety
  float ws  = _app.getCurrentWindSpeed(),
        rt  = _app.getReleaseThresholdValue();
  byte pos  = _app.getShutterPosition();
  bool res = (ws < rt) && (pos == App::shutterPostionSafety);
  if (res){
    Serial.println("condtion2");
  }
  return res;
}
bool ShutterActionMgr::_condition3(unsigned long now) const{
  // ws <= Release Threshold AND Releasing AND (now-rtpT)>= mtB
  float ws = _app.getCurrentWindSpeed(),
        rt = _app.getReleaseThresholdValue();
  byte pos = _app.getShutterPosition();
  bool res  =  (ws < rt) && 
               (pos ==  App::shutterPostionReleasing) && 
               ((now - _releaseThresholdPassingTime) >= _minTimeBelowReleaseThresholdForAction);
  if (res){
    Serial.println("condition3");
  }
  return  res;
}

void ShutterActionMgr::_resetSafetyAction(unsigned long now){
  _app.setShutterPosition(App::shutterPostionSafety);
  //digitalWrite(YOKIS_OPEN_PIN,HIGH);
  //_yokisPulseStartTime = now;
  //_pulsing = true;
  Serial.println("Reset Safety action!");
}

void ShutterActionMgr::_safetyAction(unsigned long now){
  _app.setShutterPosition(App::shutterPostionSafety);
  digitalWrite(YOKIS_OPEN_PIN,HIGH);
  _yokisPulseStartTime = now;
  _pulsing = true;
  Serial.println("Safety action!");
}
void ShutterActionMgr::_releaseAction(unsigned long now){
  _app.setShutterPosition(App::shutterPositionRelease);
  digitalWrite(YOKIS_CLOSE_PIN,HIGH);
  _yokisPulseStartTime = now;
  _pulsing = true;
   Serial.println("Release action!");
}
void ShutterActionMgr::_resetThresholdCrossingTimeAction(unsigned long now){
  _releaseThresholdPassingTime = now;
  _app.setShutterPosition(App::shutterPostionReleasing); 
  Serial.println("Reset Threshold crossing, Releasing");
}

void ShutterActionMgr::_unPulseAction(unsigned long now){
  digitalWrite(YOKIS_OPEN_PIN,LOW);
  digitalWrite(YOKIS_CLOSE_PIN,LOW);
  _pulsing = false;
  _noActionDelay = true;
  _yokisPulseStartTime = now;
  Serial.println("unpulse action!");
}

void ShutterActionMgr::_unHoldAction(unsigned long now){
  _noActionDelay = false;
  Serial.println("unHold action!");
}

void ShutterActionMgr::_noAction(unsigned long now){
}

void ShutterActionMgr::_update(long unsigned now){
  for (byte i=0;i< _nbConditions;i++){
    if ((this->*_caPairVec[i].cfPtr)(now)){
      (this->*_caPairVec[i].afPtr)(now);
      //_sayMyName(String("Doing caPair: ") + String(i));
      break;
    }
  }
  
}  // defined here!
void ShutterActionMgr::_shutterActionMgrFunction(){
}

ShutterActionMgr::ShutterActionMgr(App &_app,String name, long unsigned updatePeriod):Updateable(_app,name, updatePeriod){
}
bool ShutterActionMgr::isPulsing() const{
  return _pulsing;  
}
