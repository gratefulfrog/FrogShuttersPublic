#ifndef COMMS_MGR_H
#define COMMS_MGR_H

#include <Arduino.h>
#include <FrogSendGmailLib.h>
#include "Updateable.h"
#include "arduino_secrets.h"
#include "MessageFormats.h"
#include "Tools.h"
#include "Config.h"


class App;

class CommsMgr: public Updateable{
  public:
    static const int mTypeAlert  = 0,
                     mTypeStatus = 1,
                     mTypeAction = 2,
                     mTypeBoot   = 3,
                     nbMType     = 4;
    static  String subjectStringVec[nbMType],
                   messageFormatVec[nbMType];
    static void init();
    
  protected:
    static const int maxMailAttempts = 8; //=2^9-1 seconds! 32 seconds wait, 511 seconds total try time fix this with updateable class!
    static bool inited; 
    static const char *_toAddress,
                      *_toName;
    String             _outgoingMsgBody;
    FrogSendGmail     *_fsg;
    
    virtual void _update(long unsigned now);
    
    const char *_getSubject(int mtype) const;
    const char *_getMsgBody(int mtype); 
    const char *_getMsgText(const String format);

    int _currentMsgType = mTypeStatus;

  public:
    CommsMgr(App &app,
             String name = "The COMMS Manager", 
             long unsigned _updatePeriod = COMMS_MGR_POLL_PERIOD);
    int send(int mtype);
    void setNewShutterActionFlag(bool doIt = true);
};


/*
void setup(){
  configStartup();
  App ap = App();
  CommsMgr cmr = CommsMgr(&ap);
 
  for (int i = 0; i< CommsMgr::nbMType;i++){
    Serial.println((i == 0 ? "First send..." : (i==1 ? "Second send..." : "Third send...")));
    while(cmr.send(i));
    if(i<CommsMgr::nbMType-1){
      Serial.println("Waiting a few seconds before the next send...");
      delay(3000);
    }
  }
  Serial.println("Done");
 
}
 */

#endif
