
#include "MessageFormats.h"

const String wsRS      = String("%ws"),
             lastNZRS  = String("%lasNZWST"), 
             spRS      = String("%sp"),
             stRS      = String("%st"),
             rtRS      = String("%rt");

const String SubjectPrefix =  String("FrogShutters : ");


const String SubjectAlert      = SubjectPrefix + String(SUBJECT_ALERT_STRING),
             SubjectReport     = SubjectPrefix + SUBJECT_REPORT_STRING,
             SubjectAction     = SubjectPrefix + SUBJECT_ACTION_STRING,
             SubjectBoot       = SubjectPrefix + SUBJECT_BOOT_STRING,
             htmlProlog        = String("Mime-Version: 1.0\n")
                               + String("Content-Type: text/html; charset=\"ISO-8859-1\"\n") 
                               + String("Content-Transfer-Encoding: 7bit\n") 
                               + String("<html><body>\n"),
             htmlPostlog       = String("</table></body></html>\n"),
             htmlAlarm         = String(HTML_ALARM_STRING),
             htmlStatus        = String(HTML_STATUS_STRING),
             htmlAction        = String(HTML_ACTION_STRING),
             htmlWindSpeedP    = String(HTML_WINDSPEED_STRING)         + wsRS      + String("</b> m/s.</td></tr>"),
             htmlLastNonZeroP  = String(HTML_LAST_NON_ZERO_STRING)     + lastNZRS  + String("</b></td></tr>"),
             htmlShutterPosP   = String(HTML_SHUTTER_POSTION_STRING)   + spRS      + String("</b></td></tr>"),
             htmlSafetyThldP   = String(HTML_SAFETY_THRESHOLD_STRING)  + stRS      + String("</b> m/s.</td></tr>"),
             htmlReleaseThldP  = String(HTML_RELEASE_THRESHOLD_STRING) + rtRS      + String("</b> m/s.</td></tr>");


const String MsgAlertFormat  = htmlProlog
                             + htmlAlarm
                             + htmlShutterPosP 
                             + htmlWindSpeedP
                             + htmlSafetyThldP
                             + htmlReleaseThldP
                             + htmlLastNonZeroP
                             + htmlPostlog
                             ;
                            
const String MsgReportFormat = htmlProlog
                             + htmlStatus
                             + htmlShutterPosP 
                             + htmlWindSpeedP
                             + htmlSafetyThldP
                             + htmlReleaseThldP
                             + htmlLastNonZeroP
                             + htmlPostlog
                             ;

const String MsgActionFormat = htmlProlog
                             + htmlAction
                             + htmlShutterPosP 
                             + htmlWindSpeedP
                             + htmlSafetyThldP
                             + htmlReleaseThldP
                             + htmlLastNonZeroP
                             + htmlPostlog
                             ;
