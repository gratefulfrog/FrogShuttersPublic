#include "ThresholdMgr.h"
#include "App.h"

void ThresholdMgr::_update(long unsigned now){
  int localCountL = 0,
    localCountR = 0;
  String s = " Encoder: ";
  if (_encoderPtrL->update(localCountL)){
    _app.setReleaseThresholdValue(_app.getReleaseThresholdValue() + localCountL);
    s += String("Release: ") + (String(_app.getReleaseThresholdValue()));
    _app.updateLCD(true);
    _sayMyName(s);
  }
  if (_encoderPtrR->update(localCountR)){
    _app.setSafetyThresholdValue(_app.getSafetyThresholdValue() + localCountR);
    s += String("Safety: ") + String(_app.getSafetyThresholdValue());
    _app.updateLCD(true);
    _sayMyName(s);
  }      
}  

void ThresholdMgr::_thresholdMgrFunction(){
}

ThresholdMgr::ThresholdMgr(App &_app,String name, long unsigned updatePeriod):Updateable(_app,name, updatePeriod){
  // _encoderPtrL = new InterruptMgr(KEY_LEFT_PIN,SIG_LEFT_PIN,DIR_LEFT_PIN);  //buttonPinL,signalPinL,dirPinL);
  //_encoderPtrR = new InterruptMgr(KEY_RIGHT_PIN,SIG_RIGHT_PIN, DIR_RIGHT_PIN); //buttonPinR,signalPinR,dirPinR);
  _encoderPtrL = new HWDKRE(SIG_LEFT_PIN,DIR_LEFT_PIN,KEY_LEFT_PIN);  //buttonPinL,signalPinL,dirPinL);
  _encoderPtrR = new HWDKRE(SIG_RIGHT_PIN, DIR_RIGHT_PIN,KEY_RIGHT_PIN); //buttonPinR,signalPinR,dirPinR);
}
