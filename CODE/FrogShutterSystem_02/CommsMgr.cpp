#include "CommsMgr.h"
#include "App.h"

String CommsMgr::subjectStringVec[nbMType];
String CommsMgr::messageFormatVec[nbMType];
bool CommsMgr::inited = false;
 
void CommsMgr::init(){
  if (inited){
    return;
  }
  subjectStringVec[0] = SubjectAlert;
  subjectStringVec[1] = SubjectReport;
  subjectStringVec[2] = SubjectAction;
  subjectStringVec[3] = SubjectBoot;
  messageFormatVec[0] = MsgAlertFormat;
  messageFormatVec[1] = MsgReportFormat;
  messageFormatVec[2] = MsgActionFormat;
  messageFormatVec[3] = MsgReportFormat;
  inited = true;
}                                      

const char *CommsMgr::_toAddress   = "gratefulfrog@gmail.com",
           *CommsMgr::_toName      = "The Sincerely Grateful Frog";

const char *CommsMgr::_getSubject(int mtype) const{
    return subjectStringVec[mtype].c_str();
}

void CommsMgr::_update(long unsigned now){
  _sayMyName("and I'm updating!");
  send(_currentMsgType);
}

const char *CommsMgr::_getMsgBody(int mtype){
  return _getMsgText(messageFormatVec[mtype]);
}

const char *CommsMgr::_getMsgText(const String format){ 
  _outgoingMsgBody = format;  // res must be at file scope!
  _outgoingMsgBody.replace(wsRS, String(_app.getCurrentWindSpeed()));
  _outgoingMsgBody.replace(lastNZRS, millisToHHMMSS(_app.getLastNonZeroWSInterval()));
  _outgoingMsgBody.replace(spRS,_app.getShutterPositionName());
  _outgoingMsgBody.replace(stRS,String(_app.getSafetyThresholdValue()));
  _outgoingMsgBody.replace(rtRS,String(_app.getReleaseThresholdValue()));
  
  return _outgoingMsgBody.c_str();
}

CommsMgr::CommsMgr(App &app,
                   String name, 
                   long unsigned updatePeriod):
  Updateable(app, name, updatePeriod){
  init();
  _fsg = new FrogSendGmail(SECRET_WIFI_SSID,
                           SECRET_WIFI_PASS,
                           SECRET_DOMAIN,
                           SECRET_SEND_ACCOUNT,
                           SECRET_SEND_ACCOUNT_PASSWORD,
                           SECRET_SEND_NAME);
                           
}
int CommsMgr::send(int mtype){
  static int tryCount = 0;
  int err0 =0;
  _currentMsgType = mtype;
  _app.lcdEmailMsg(true);
  delay(500);
  err0 = _fsg->connectWifiAndSendSMTPGmail(_getSubject(_currentMsgType), 
                                           _getMsgBody(_currentMsgType), 
                                           _toAddress, 
                                           _toName);
  Serial.println(String(FrogSendGmail::errorMessageVec[err0]));
  // binary exponential backoff
  if (err0 && tryCount < maxMailAttempts){
    int del = pow(2,tryCount++);
    Serial.println(String("Backing off ") + String(del) + (del < 2 ? " second" : " seconds")); 
    _updatePeriod = (1000*del); 
  }
  else{
    // give up
    _updatePeriod = COMMS_MGR_POLL_PERIOD;
    tryCount = 0;
    _currentMsgType = mTypeStatus;
  }
  _app.lcdEmailMsg(false);
  return err0;
}
void CommsMgr::setNewShutterActionFlag(bool doIt){
  if(doIt){
    _currentMsgType=mTypeAction;
    _updatePeriod = 0;
  }
  else{
    _currentMsgType=mTypeStatus;
    _updatePeriod = COMMS_MGR_POLL_PERIOD;
  }
}
