#include "TestMgr.h"
#include "App.h"

String TestMgr::t0[2]    = {TEST_STARTUP_0_STRING, TEST_STARTUP_1_STRING},         // index 0: print, not press, next = 1
       TestMgr::t1[2][2] = {{TEST_SHORT_PRESS_STRING, TEST_CONTINUE_STRING},       // index 1: cycle, wait on press next = 2, or 4
                   {TEST_LONG_PRESS_STRING, TEST_CANCEL_STRING}},
       TestMgr::t2[2]    = {TEST_POSITION_SAFETY_0_STRING, TEST_POSITION_SAFETY_1_STRING},      // index 2: print, wait on press next = 3, or 4
       TestMgr::t3[2]    = {TEST_POSITION_RELEASED_0_STRING, TEST_POSITION_RELEASED_1_STRING},  // index 3: print, wait on press next = 5, or 4
       TestMgr::t4[2]    = {TEST_SHORT_PRESS_STRING, TEST_CONFIRM_CANCEL_STRING},      // index 4: print, wait on press next = 5, or previous next
       TestMgr::t5[2][2] = {{TEST_COMPLETE_STRING, ""},                    // index 5: cycle, no press
                   {TEST_OPERATIONAL_0_STRING, TEST_OPERATIONAL_1_STRING}},
       TestMgr::r0       = TEST_SECOND_STRING,
       TestMgr::r1       = TEST_SECONDS_STRING,
       TestMgr::done     = TEST_DONE;
       
String TestMgr::sStringVec[][2] = {t0,t2,t3,t4},
       TestMgr::dStringVec[][2][2] = {t1,t5}; 

void TestMgr::_update(long unsigned now){
  String s = "and I am";

  byte pressType;
  bool pressed  =  _tpb->getType(pressType),
       bRunTests = false;
  if (pressed){
    s+= " turning on the LCD";
    _app.updateLCD(true);
    if (pressType == TimedPB::ShortPress){
      s+= " and seting shutter position to unknown";
      _app.setShutterPosition();
    }
    else{
      s+= " and Starting Tests";
      bRunTests = true;
      //cycleTestPhase();
      //s+= " new phase: " + String(_testPhase);
    }
    _sayMyName(s);
  }
  if(bRunTests){
    _runTests();
  }
}
void TestMgr::_setShutterPosition(byte positionType){
  switch(positionType){
    case App::shutterPostionSafety:
      digitalWrite(YOKIS_OPEN_PIN,HIGH);
      delay(SHUTTER_ACTION_MGR_YOKIS_PIN_TIME);
      digitalWrite(YOKIS_OPEN_PIN,LOW);
      break;
    case App::shutterPositionRelease:
      digitalWrite(YOKIS_CLOSE_PIN,HIGH);
      delay(SHUTTER_ACTION_MGR_YOKIS_PIN_TIME);
      digitalWrite(YOKIS_CLOSE_PIN,LOW);
      break;
    default:
      digitalWrite(YOKIS_OPEN_PIN,LOW);
      digitalWrite(YOKIS_CLOSE_PIN,LOW);
      break;    
  }
  _app.setShutterPosition(positionType);
  delay(2000);
}

bool TestMgr::_getPress(bool &ret){
  byte pressType;
  bool pressed = _tpb->getType(pressType);

  if (pressed){
    ret = pressType == TimedPB::ShortPress;
  }
  return pressed;
}
    

void TestMgr::_f1(){
  _app.lcdDisplayMsg(t0[0],t0[1]);
  delay(2000);
  _currentFuncIndex++;
}

void TestMgr::_f2(){  // ind: 1 or 5
  int j = (_currentFuncIndex == 1 ? 0 : 1),
      i = 0;
  bool shortPress;
  do{
    _app.lcdDisplayMsg(dStringVec[j][i][0],dStringVec[j][i][1]);
    delay(2000);
    i^=1;
  }
  while(!_getPress(shortPress));
  _currentFuncIndex = (shortPress ? 1+_currentFuncIndex : 4);
}

void TestMgr::_f3(){
  static const byte spVec[] = {App::shutterPostionSafety,
                               App::shutterPositionRelease};
  bool shortPress;
  _app.lcdDisplayMsg(sStringVec[_currentFuncIndex-1][0],
                     sStringVec[_currentFuncIndex-1][1]);
  while(!_getPress(shortPress));
  _setShutterPosition(spVec[_currentFuncIndex-2]);
  
  int nextFuncIndex = (_currentFuncIndex == 2 ? 3 : 5);
  _currentFuncIndex = (shortPress ? nextFuncIndex : 4);
}

void TestMgr::_f4(){
  bool shortPress;
  _app.lcdDisplayMsg(t4[0],t4[1]);
  while(!_getPress(shortPress));
  _currentFuncIndex = (shortPress ? 1+_currentFuncIndex: _prevInd);
}

void TestMgr::_f5(){
  bool shortPress;
  int timeLeft = 30;
  unsigned long now = millis();
  char buff[17];

  _app.setShutterPosition();
  delay(2000);
  _app.lcdDisplayMsg(dStringVec[1][0][0],dStringVec[1][0][1]);
  delay(1000);
  do{
    sprintf(buff,t5[1][1].c_str(),timeLeft);
    buff[16] = '\0';
    _app.lcdDisplayMsg(dStringVec[1][1][0],String(buff));
    delay(1000);
    if (millis()-now>=1000){
      now = millis();
      if(!timeLeft--){
        _currentFuncIndex = -1;
        return;
      }
      if (timeLeft == 1){
        t5[1][1].replace(r1,r0);
      }
      else if (timeLeft == 0){
        t5[1][1].replace(r0,r1);
      }
    }
  }
  while(!_getPress(shortPress));
  _currentFuncIndex = -1;
}

const cfPtr TestMgr::_fVec[TestMgr::_nbTestFuncs] = {&TestMgr::_f1,
                          &TestMgr::_f2,
                          &TestMgr::_f3,
                          &TestMgr::_f3,
                          &TestMgr::_f4,
                          &TestMgr::_f5};

TestMgr::TestMgr(App &_app,
		 String name,
		 long unsigned updatePeriod):Updateable(_app,name, updatePeriod){
  _tpb =  new TimedPB(PUSHBUTTON_PIN);
}

void TestMgr::_runTests(){
  int ind = 0;  
  _currentFuncIndex = 0;
  while(_currentFuncIndex>-1){
    if (_currentFuncIndex != 4){
      _prevInd = _currentFuncIndex;
    }
    (this->*_fVec[_currentFuncIndex])();
  }
  _app.lcdDisplayMsg(TestMgr::done);
}
