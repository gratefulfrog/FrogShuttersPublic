#include "TimedPB.h"

// define static member variables
volatile long unsigned TimedPB::lastActionTime;
TimedPB *TimedPB::thisPtr;

// define static member methods
void TimedPB::onFall(){
  lastActionTime = millis();
  //thisPtr->_lastPressLengthMS = millis()-lastActionTime;
  //thisPtr->_wasPressed = true;
  attachInterrupt(thisPtr->_interruptID, TimedPB::onRise, RISING);
}
void TimedPB::onRise(){
  //lastActionTime = millis();
  //thisPtr->_wasPressed = false;
  thisPtr->_lastPressLengthMS = millis()-lastActionTime;
  thisPtr->_wasPressed = true;
  attachInterrupt(thisPtr->_interruptID, TimedPB::onFall, FALLING);
}

// define instance methods
TimedPB::TimedPB(byte pin):_interruptID(digitalPinToInterrupt(pin)){
  // initalize static member variables
  TimedPB::thisPtr = this;
  TimedPB::lastActionTime = millis();
  _lastPressLengthMS = 0;
  _wasPressed = false;
  
  //attachInterrupt(_interruptID, TimedPB::onRise, RISING);
  attachInterrupt(_interruptID, TimedPB::onFall, FALLING);
  
}
unsigned long TimedPB::getPressLengthMS(){
  return _lastPressLengthMS;
}

bool TimedPB::getType(byte &resType){
  unsigned long pressTime;
  if (_wasPressed){
    resType = (_lastPressLengthMS < shortMaxTime) ? ShortPress :  LongPress;
    _wasPressed = false;
    //Serial.println("was pressed: " + String(resType) + " " + String(_lastPressLengthMS) + "  " + String(shortMaxTime));
    return true;
  }
  return false;
}
