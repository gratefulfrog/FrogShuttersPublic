#ifndef TIMEDPB_H
#define TIMEDPB_H

#include <Arduino.h>
#include "Config.h"

class TimedPB{
  public:
    volatile static long unsigned lastActionTime;
    static void onFall();
    static void onRise();
    static TimedPB *thisPtr;
    static const long unsigned shortMaxTime = TIMED_PB_MAX_SHORT_TIME;
    static const byte ShortPress = 0,
                      LongPress  = 1;
    
  protected:
    const byte             _interruptID;
    volatile unsigned long _lastPressLengthMS;
    volatile bool          _wasPressed;

 public:
    TimedPB(byte pin);
    unsigned long getPressLengthMS();
    bool getType(byte &resType);
};

#endif
