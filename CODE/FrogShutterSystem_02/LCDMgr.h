#ifndef LCDMGR_H
#define LCDMGR_H

#include <Arduino.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

#include "Config.h"
#include "Updateable.h"
#include  "Tools.h"

class LCDMgr: public Updateable{
  protected:
    static const String        _floatFormatString,
                               _charFormatString,
                               _windSpeedUnitString;
    static const int           _floatFormatCharArrayLength;
    static const char          _trueChar,
                               _falseChar;
    static const long unsigned _LCDTimeout;
  
    LiquidCrystal_I2C *_lcd;
    
    const uint8_t _adr,
                  _nbCols,
                  _nbLines;
    
    bool           _backlightOn = true;
    unsigned long  _lastBacklightOnTime = 0;
    
    void _startupMessage();
    virtual void _update(long unsigned now);
    void _backlightEnable(bool yes = true);
    
  public:
    LCDMgr(App &app,
           uint8_t adr     = LCD_I2C_ADDRESS, 
           uint8_t nbCols  = LCD_NB_COLS, 
           uint8_t nbLines = LCD_NB_ROWS,
           String name = "The LCD Manager", 
           long unsigned updatePeriod = LCD_MGR_POLL_PERIOD);
    void displayMsg(String line0, String line1,bool forceOn);
    void displayMsg(String msg, bool forceOn);
    void emailDisplay(bool yes);
    void updateDisplay(bool forceOn = false);
    void alarmDisplay(bool yes);
};

#endif
