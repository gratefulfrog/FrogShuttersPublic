#ifndef WINDSPEED_MGR_H
#define WINDSPEED_MGR_H

#include "Config.h"
#include "Updateable.h"
#include "EltakoWindSensor.h"

class WindSpeedMgr: public Updateable{
  // get the new windspeed every Minute
  public:
    static WindSpeedMgr *thisPtr;
    
    volatile long unsigned pulseCount = 0;
    long unsigned anemometerLastBounceTime = millis();

    // ISR for Anemometer
    static void anemometerPulse();
    static const long unsigned anemometerDebounceDelay = 10;

    static const byte   currentWSIndex        = 0,
                        lastWSIndex           = 1,
                        nbVecElts             = 2;
    
  protected:
    unsigned long _lastPulseTime     = millis(),
                  _lastPulseCount    = 0,
                  _lastNonZeroWSTime = 0;

    EltakoWindSensor *_ews;
                  
    float _windSpeedVec[nbVecElts] = {0.0,0.0};

    virtual void _update(long unsigned now);
    void _windSpeedMgrFunction(long unsigned now);

  public:
    WindSpeedMgr(App &_app,String name = "The Wind Speed Manager", long unsigned updatePeriod = WIND_SENSOR_POLL_PERIOD); 
    unsigned long getLastNonZeroWSTime() const;
    unsigned long getLastNonZeroWSInterval(unsigned long now) const;
    float         getWindSpeed(byte index = currentWSIndex) const;

};

#endif
