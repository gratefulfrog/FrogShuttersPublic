#include "App.h"
#include "EEPROMMgr.h"

const String App::shutterPostionNameVec[] ={ UNKNOWN_STRING, 
                                             SAFETY_STRING, 
                                             RELEASING_STRING, 
                                             RELEASED_STRING};

String App::getShutterPositionName() const{
  return shutterPostionNameVec[_shutterPosition];
}
byte App::getShutterPosition() const{
  return _shutterPosition;
}
void  App::setShutterPosition(byte pos){
  _shutterPosition = pos; 
  _ssMgr->updateShutterPos(pos);
  switch(_shutterPosition){
    case shutterPositionUnknown:
      digitalWrite(_ledOpenPin,LOW);
      digitalWrite(_ledClosePin,LOW);
      _commsMgr->setNewShutterActionFlag(false);
      break;
    case shutterPostionSafety:
      digitalWrite(_ledOpenPin,HIGH);
      digitalWrite(_ledClosePin,LOW);
      _commsMgr->setNewShutterActionFlag();
      break;
    case shutterPostionReleasing:
      digitalWrite(_ledOpenPin,HIGH);
      digitalWrite(_ledClosePin,HIGH);
      _commsMgr->setNewShutterActionFlag();
      break;
    case shutterPositionRelease:
      digitalWrite(_ledOpenPin,LOW);
      digitalWrite(_ledClosePin,HIGH);
      _commsMgr->setNewShutterActionFlag();
      break;   
  } 
}
float App::getReleaseThresholdValue() const{
  return _thresholdTimeStructVec[releaseThresholdIndex].threshold;
}
void App::setReleaseThresholdValue(float newVal) {
  _thresholdTimeStructVec[releaseThresholdIndex].threshold = max(0,min(newVal,
                                                                       _thresholdTimeStructVec[safetyThresholdIndex].threshold-1));
  _ssMgr->updateReleaseThreshold(newVal);
}
unsigned long App::getReleaseThresholdTime() const{
  return _thresholdTimeStructVec[releaseThresholdIndex].time;
}
float App::getSafetyThresholdValue() const{
  return _thresholdTimeStructVec[safetyThresholdIndex].threshold;
}
void App::setSafetyThresholdValue(float newVal) {
  _thresholdTimeStructVec[safetyThresholdIndex].threshold = max(1+_thresholdTimeStructVec[releaseThresholdIndex].threshold,newVal);
  _ssMgr->updateSafetyThreshold(newVal);
}
unsigned long App::getSafetyThresholdTime() const{
  return _thresholdTimeStructVec[safetyThresholdIndex].time;
}
unsigned long App::getLastNonZeroWSInterval() const{
  return _windSpeedMgr->getLastNonZeroWSInterval(millis());
}
float App::getCurrentWindSpeed(bool useCurrent) const{
  return _windSpeedMgr->getWindSpeed(useCurrent ? WindSpeedMgr::currentWSIndex: WindSpeedMgr::lastWSIndex);
}

void App::_initializeState(){
  // recover last shutter postion from EEPROM
  _shutterPosition = _ssMgr->getShutterPosition();
  _shutterPosition = 0;
  
  // then get thresholds from EEPRM
  float tempThresholdVec[] = {_ssMgr->getReleaseThreshold(), _ssMgr->getSafetyThreshold()};
  //float tempThresholdVec[] = {10,11};
  
  for (byte i = 0;i<App::nbVecElts;i++){
    if(tempThresholdVec[i]!=0.0){ // if there was a value in EEPROM, then use it
      _thresholdTimeStructVec[i].threshold = tempThresholdVec[i];
    }
    else{
      int i=0;
      // use default and update EEPROM
      _ssMgr->updateIndexedThreshold(i,_thresholdTimeStructVec[i].threshold);     
    }
  }
  Serial.println("Shutter Position: "  + getShutterPositionName());
  Serial.println("Release Threshold: " + String(getReleaseThresholdValue()));
  Serial.println("Safety Threshold: "  + String(getSafetyThresholdValue()));
}

void App::_initializePins(){
  byte i=0;
  while (_outputPinAdrVec[i]){
    pinMode(*_outputPinAdrVec[i],OUTPUT);
    digitalWrite(*_outputPinAdrVec[i++],LOW);
    //Serial.println("pinmode pin: " + String(*_outputPinAdrVec[i-1]));
  }
  i=0;
  while (_inputNoPullPinAdrVec[i]){
    pinMode(*_inputNoPullPinAdrVec[i++],INPUT);
    //Serial.println("pinmode pin: " + String(*_inputNoPullPinAdrVec[i-1]));
  }
  i=0;
  while (_inputPullUpPinAdrVec[i]){
    pinMode(*_inputPullUpPinAdrVec[i++],INPUT_PULLUP);
    //Serial.println("pinmode pin: " + String(*_inputPullUpPinAdrVec[i-1]));
  }
}

void App::_initializeMgrs(){
  _testMgr            = new TestMgr(*this); 
  _windSpeedMgr       = new WindSpeedMgr(*this);
  _shutterActionMgr   = new ShutterActionMgr(*this);
  _thresholdMgr       = new ThresholdMgr(*this);
  _alarmMgr           = new AlarmMgr(*this); 
  _commsMgr           = new CommsMgr(*this);
  _lcdMgr             = new LCDMgr(*this);    // note this is NOW an updateable!!
  _ssMgr              = new ShutterStorageMgr(); 

  _updateableMgrAdrVec =  new Updateable*[_nbMgrs+1];
  
  _updateableMgrAdrVec[0] = _testMgr;
  _updateableMgrAdrVec[1] = _windSpeedMgr;
  _updateableMgrAdrVec[2] = _shutterActionMgr;
  _updateableMgrAdrVec[3] = _thresholdMgr;
  _updateableMgrAdrVec[4] = _alarmMgr;    
  _updateableMgrAdrVec[5] = _commsMgr;
  _updateableMgrAdrVec[6] = _lcdMgr,
  _updateableMgrAdrVec[7] = NULL;
}

void App::_doPulse() const{
  digitalWrite(_ledPulsePin,digitalRead(_windSensorPin));
}

App::App():
  _thresholdTimeStructVec({{RELEASE_THRESHOLD_DEFAULT,0},
                           {SAFETY_THRESHOLD_DEFAULT,0}}){
  
  Wire.begin();
  
  #ifdef EEPROM_INIT
  Serial.println("Initializing the EEPROM");
  _ssMgr->init();
  Serial.println("Done, waiting...");
  while(1);
  #endif
  
  // note the order of initializations is important! 
  _initializePins();
  _initializeMgrs();
  _initializeState();
  _commsMgr->send(CommsMgr::mTypeBoot);  // to get a status power up message
  Serial.println("Now looping...");  
}

void App::mainLoop(){
  static byte i=0;
  
  _updateableMgrAdrVec[i]->update(millis());
  if (!_shutterActionMgr->isPulsing()){
    i++;
    i = _updateableMgrAdrVec[i] ? i : 0;
  }
  _doPulse();
}
void App::updateLCD(bool forceOn ){
  _lcdMgr->updateDisplay(forceOn);
}
void App::lcdEmailMsg(bool yes){
  _lcdMgr->emailDisplay(yes);
}
void App::lcdDisplayMsg(String line0, String line1,bool forceOn){
  _lcdMgr->displayMsg(line0, line1,forceOn);
}
void App::lcdDisplayMsg(String msg, bool forceOn){
  _lcdMgr->displayMsg(msg,forceOn);
}

void App::giveAlarm(){
  // send email alarm message
  // Set LCD to ALARM !!
  // flash leds and buzzer forever
  _commsMgr->send(CommsMgr::mTypeAlert);
  
  bool shine = true;   
  while(true){
    if (shine){
      tone(_buzzerPin, ALARM_BUZZER_FREQ);
    }
    else{
      noTone(_buzzerPin);
    }
    _lcdMgr->alarmDisplay(shine);
    digitalWrite(_ledOpenPin,shine);
    digitalWrite(_ledClosePin,shine);
    digitalWrite(_ledPulsePin,shine);
    delay(ALARM_FLASH_PAUSE);
    shine=!shine;
  }
}
