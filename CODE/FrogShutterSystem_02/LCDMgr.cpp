#include "LCDMgr.h"
// inclusion here to avoid conflicting header files
#include "App.h"

const String LCDMgr::_floatFormatString = String("%04.1f"),
             LCDMgr::_charFormatString  = String("%c"),
             LCDMgr::_windSpeedUnitString = WINDSPEED_UNIT_STRING;
const int    LCDMgr::_floatFormatCharArrayLength = 5;
const long unsigned LCDMgr::_LCDTimeout  = LCD_BACKLIGHT_TIMEOUT;

void LCDMgr::_startupMessage(){
  _lcd->clear();
  _backlightEnable();
  _lcd->setCursor(0,0);
  _lcd->print(STARTUP_MSG_STRING.c_str());
  _lcd->setCursor(0,1);
  _lcd->print("      :-D       ");
}
void LCDMgr::_update(long unsigned now){
  if(_backlightOn && (now-_lastBacklightOnTime > _LCDTimeout)){
    _backlightEnable(false);
  }
}

LCDMgr::LCDMgr(App &app,
               uint8_t adr,
               uint8_t nbCols, 
               uint8_t nbLines,
               String name, 
               long unsigned updatePeriod): 
               Updateable(app, name, updatePeriod),
              _adr(adr),_nbCols(nbCols), _nbLines(nbLines){
  _lcd = new LiquidCrystal_I2C(_adr,_nbCols,_nbLines);
  _lcd->init();
  _lcd->clear();
  _startupMessage();
  delay(3000);
  updateDisplay(true);
 }
 
void LCDMgr::_backlightEnable(bool yes){
  if (yes){
    _lcd->backlight();
    _lastBacklightOnTime = millis();
  }
  if( !yes){
    _lcd->noBacklight();
  }
  _backlightOn = yes;
  
}

void LCDMgr::displayMsg(String line0, String line1,bool forceOn){
  String lineVec[] = {line0, line1};
  _lcd->clear();
  if (forceOn){
    _backlightEnable(true);
  }
  for (byte i=0;i<_nbLines;i++){
    _lcd->setCursor(0,i);
    _lcd->print(lineVec[i]);
  }
}


void LCDMgr::displayMsg(String msg, bool forceOn){
  displayMsg(msg.substring(0,_nbCols),msg.substring(_nbCols),forceOn);
}

void LCDMgr::emailDisplay(bool yes){
  if (yes){
    displayMsg(LCD_EMAIL_MSG_L0,LCD_EMAIL_MSG_L1,false);
  }
  else{
    updateDisplay();
  }
}

void LCDMgr::updateDisplay(bool forceOn){     // use _app reference to get values to display...
  static const int spaceAvailable =  _nbCols - _floatFormatCharArrayLength - _windSpeedUnitString.length();
  
  char speedS[_floatFormatCharArrayLength],
       releaseThresholdS[_floatFormatCharArrayLength],
       safetyThresholdS[_floatFormatCharArrayLength];
  sprintf(speedS,
          _floatFormatString.c_str(),
          _app.getCurrentWindSpeed());
  sprintf(releaseThresholdS,
          _floatFormatString.c_str(),
          _app.getReleaseThresholdValue());
  sprintf(safetyThresholdS,
          _floatFormatString.c_str(),
          _app.getSafetyThresholdValue());

  String timeString = millisToHHMMSS(_app.getLastNonZeroWSInterval());
  spaceTimeString (timeString,spaceAvailable);

  byte shutterPos = _app.getShutterPosition();
  String releaseS = ((shutterPos > _app.shutterPostionSafety) ? TRUE_CHAR_STRING : FALSE_CHAR_STRING),
         safetyS  = (((shutterPos == _app.shutterPostionSafety) || 
                     (shutterPos == _app.shutterPostionReleasing)) ? TRUE_CHAR_STRING : FALSE_CHAR_STRING);
  
  String line1  = String(speedS) + _windSpeedUnitString + timeString,
         line2 = String(releaseThresholdS) + " " + String(safetyThresholdS) + "   " + 
                 releaseS +  
                 "  " + 
                 safetyS;

  displayMsg(line1,line2,forceOn);
}

void LCDMgr::alarmDisplay(bool yes){
  if (yes){
    displayMsg(LCD_ALARM_MSG_L0,LCD_ALARM_MSG_L0,true);
  }
  else{
    _lcd->clear();
  }
}
