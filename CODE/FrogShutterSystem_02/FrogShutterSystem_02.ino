#include "App.h"

App *app;

void setup() {
  Serial.begin(115200);
  app = new App();
}

void loop() {
  app->mainLoop();
}
