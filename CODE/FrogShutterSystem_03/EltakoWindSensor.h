#ifndef ELTAKO_H
#define ELTAKO_H

#include <Arduino.h>
#include "Config.h"

class EltakoWindSensor{
  protected:
    volatile long unsigned     _count;
    long unsigned              _lastCount,
                               _lastCountStartTime;
    static EltakoWindSensor   *_thisPtr;  // needed for the static ISR method

  public:
    static void pulseCountISR();

    EltakoWindSensor(byte pin = WIND_SENSOR_PIN);
    float getWindSpeed(long unsigned now);
};
#endif
