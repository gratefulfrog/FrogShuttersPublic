#include "AlarmMgr.h"
#include "App.h"

void AlarmMgr:: _update(long unsigned now){
    _sayMyName("and I'm updating!");
  if (_app.getLastNonZeroWSInterval() > ALARM_MGR_MAX_NON_ZERO_WS_INTERVAL){
    _app.giveAlarm();
  }
} 

AlarmMgr::AlarmMgr(App &_app,
              		 String name,
              		 long unsigned updatePeriod):Updateable(_app,name, updatePeriod){}
