#ifndef VOCAB_FR_H
#define VOCAB_FR_H

#include <Arduino.h>

////////////////////////////////////////////
//////////// Language = Frenc ///////////////
////////////////////////////////////////////

#define _STARTUP_MSG         ("Mise En Route !!")
#define _TRUE_CHAR           ('V') 
#define _FALSE_CHAR          ('F')
#define _LCD_EMAIL_MSG_L0    ("Envoi d'email...") 
#define _LCD_EMAIL_MSG_L1    ("Maintenant !") 
#define _LCD_ALARM_MSG_L0    ("  A L E R T E ! ") 

// App.pp
#define _UNKNOWN          ("Inconnu")
#define _SAFETY           ("Sécurité")
#define _RELEASING        ("En cours de libération")
#define _RELEASED         ("Libéré")

// MessageFormats.cpp
#define  _SUBJECT_ALERT_STRING            ("ALERTE!!")
#define  _SUBJECT_REPORT_STRING           ("Rapport de Situation")
#define  _SUBJECT_ACTION_STRING           ("Rapport d'Action")
#define  _SUBJECT_BOOT_STRING             ("Démarrage du Système!")
#define  _HTML_ALARM_STRING               ("<h2>FrogShutters ALERTE !</h2><table>")
#define  _HTML_STATUS_STRING              ("<h2>FrogShutters Rapport de Situtation</h2><table>")
#define  _HTML_ACTION_STRING              ("<h1>FrogShutters Rapport de Nouvelle Action</h1><table>")
#define  _HTML_WINDSPEED_STRING           ("<tr><td>Vitesse Actuelle du Vent</td><td>:</td><td><b>")
#define  _HTML_MAX_WINDSPEED_STRING       ("<tr><td>Vitesse Maximale du Vent depuis dernier rapport</td><td>:</td><td><b>")
#define  _HTML_LAST_NON_ZERO_STRING       ("<tr><td>Temps depuis dernière Mesure Non Zéro du Vent</td><td>:</td><td><b>")
#define  _HTML_SHUTTER_POSTION_STRING     ("<tr><td>Etat Actuel des Volets</td><td>:</td><td><b>")
#define  _HTML_SAFETY_THRESHOLD_STRING    ("<tr><td>Seuil de Sécurité Actuel</td><td>:</td><td><b>")
#define  _HTML_RELEASE_THRESHOLD_STRING   ("<tr><td>Seuil de Libération Actuel</td><td>:</td><td><b>")

// TestMgr.cpp   // pas d'accentes sur LCD !!
#define _TEST_STARTUP_0_STRING                ("Demarrage des")
#define _TEST_STARTUP_1_STRING                ("Tests")
#define _TEST_SHORT_PRESS_STRING              ("Touche Rapide")
#define _TEST_CONTINUE_STRING                 ("pour Continuer")
#define _TEST_LONG_PRESS_STRING               ("Touche Longue")
#define _TEST_CANCEL_STRING                   ("pour Annuler")
#define _TEST_POSITION_SAFETY_0_STRING        ("Activer Position")
#define _TEST_POSITION_SAFETY_1_STRING        ("Securite, ok?")
#define _TEST_POSITION_RELEASED_0_STRING      ("Activer Position")
#define _TEST_POSITION_RELEASED_1_STRING      ("Libere, ok?")
#define _TEST_CONFIRM_CANCEL_STRING           ("Conf. Annuler?")
#define _TEST_COMPLETE_STRING                 ("Tests Termines")
#define _TEST_OPERATIONAL_0_STRING            ("Operationnel")
#define _TEST_OPERATIONAL_1_STRING            ("dans %2d secondes")
#define _TEST_SECOND_STRING                   ("seconde")
#define _TEST_SECONDS_STRING                  ("secondes")
#define _TEST_DONE                            ("Termine!")

/*
String TestMgr::t0[2]    = {"Demarrage des", "Tests"},                 // index 0: print, not press, next = 1
       TestMgr::t1[2][2] = {{"Touche Rapide", "pour Continuer"},       // index 1: cycle, wait on press next = 2, or 4
                   {"Touche Longue", "pour Annuler"}},
       TestMgr::t2[2]    = {"Activer Position", "Securite, ok?"},      // index 2: print, wait on press next = 3, or 4
       TestMgr::t3[2]    = {"Activer Position", "Liberee, ok?"},       // index 3: print, wait on press next = 5, or 4
       TestMgr::t4[2]    = {"Touche Rapide", "Confirm Annuler?"},      // index 4: print, wait on press next = 5, or previous next
       TestMgr::t5[2][2] = {{"Tests Termines", ""},                    // index 5: cycle, no press
                   {"Operationnel", "dans %2d secondes"}},
       TestMgr::r0       = "seconde",
       TestMgr::r1       = "secondes";
*/

/*
const String SubjectAlert      = SubjectPrefix + String("ALERTE!!"),
             SubjectReport     = SubjectPrefix + "Rapport de Situtation",
             SubjectAction     = SubjectPrefix + "Rapport d'Action",
             SubjectBoot       = SubjectPrefix + "Démarrage du Système!",
             htmlProlog        = String("Mime-Version: 1.0\n")
                               + String("Content-Type: text/html; charset=\"ISO-8859-1\"\n") 
                               + String("Content-Transfer-Encoding: 7bit\n") 
                               + String("<html><body>\n"),
             htmlPostlog       = String("</body></html>\n"),
             htmlAlarm         = String("<h2>FrogShutters ALERTE !</h2>"),
             htmlStatus        = String("<h2>FrogShutters Rapport de Situtation</h2>"),
             htmlAction        = String("<h1>FrogShutters Rapport de Nouvelle Action</h1>"),
             htmlWindSpeedP    = String("<p>Vitesse Actuelle du Vent                      : <b>") + wsRS      + String("</b> m/s.</p>"),
             htmlLastNonZeroP  = String("<p>Temps depuis dernière Mesure Non Zéro du Vent : <b>") + lastNZRS  + String("</b>.</p>"),
             htmlShutterPosP   = String("<p>Etat Actuel des Volets                        : <b>") + spRS      + String("</b>.</p>");

*/

#endif
