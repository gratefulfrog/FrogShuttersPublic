#ifndef VOCAB_EN_H
#define VOCAB_EN_H

#include <Arduino.h>

////////////////////////////////////////////
//////////// Language = English/////////////
////////////////////////////////////////////

#define _STARTUP_MSG         (" Starting Up !! ")
#define _TRUE_CHAR           ('T')
#define _FALSE_CHAR          ('F')
#define _LCD_EMAIL_MSG_L0    ("Sending email...")
#define _LCD_EMAIL_MSG_L1    ("Now !")
#define _LCD_ALARM_MSG_L0    ("  A L A R M !  ")

// App.cpp
#define _UNKNOWN          ("Unknown")
#define _SAFETY           ("Safety")
#define _RELEASING        ("Releasing")
#define _RELEASED         ("Released")

// MessageFormats.cpp
#define  _SUBJECT_ALERT_STRING            ("ALERT!!")
#define  _SUBJECT_REPORT_STRING           ("Status Report")
#define  _SUBJECT_ACTION_STRING           ("Shutter Action Report")
#define  _SUBJECT_BOOT_STRING             ("System Startup!")
#define  _HTML_ALARM_STRING               ("<h2>FrogShutters ALARM !</h2><table>")
#define  _HTML_STATUS_STRING              ("<h2>FrogShutters Status Report</h2><table>")
#define  _HTML_ACTION_STRING              ("<h1>FrogShutters New Shutter Action Report</h1><table>")
#define  _HTML_WINDSPEED_STRING           ("<tr><td>Current Wind speed</td><td>:</td><td><b>")
#define  _HTML_MAX_WINDSPEED_STRING       ("<tr><td>Maximum Wind speed since last report</td><td>:</td><td><b>")
#define  _HTML_LAST_NON_ZERO_STRING       ("<tr><td>Last Non Zero Wind Speed Measurement</td><td>:</td><td><b>")
#define  _HTML_SHUTTER_POSTION_STRING     ("<tr><td>Current Shutter Postion</td><td>:</td><td><b>")
#define  _HTML_SAFETY_THRESHOLD_STRING    ("<tr><td>Current Safety Threshold</td><td>:</td><td><b>")
#define  _HTML_RELEASE_THRESHOLD_STRING   ("<tr><td>Current Release Threshold</td><td>:</td><td><b>")

// TestMgr.cpp
#define _TEST_STARTUP_0_STRING                ("Starting Tests")
#define _TEST_STARTUP_1_STRING                ("")
#define _TEST_SHORT_PRESS_STRING              ("Short press to")
#define _TEST_CONTINUE_STRING                 ("Continue")
#define _TEST_LONG_PRESS_STRING               ("Long press to")
#define _TEST_CANCEL_STRING                   ("Cancel")
#define _TEST_POSITION_SAFETY_0_STRING        ("Do Safety")
#define _TEST_POSITION_SAFETY_1_STRING        ("Position, ok?")
#define _TEST_POSITION_RELEASED_0_STRING      ("Do Release")
#define _TEST_POSITION_RELEASED_1_STRING      ("Position, ok?")
#define _TEST_CONFIRM_CANCEL_STRING           ("Confirm Cancel?")
#define _TEST_COMPLETE_STRING                 ("Tests complete")
#define _TEST_OPERATIONAL_0_STRING            ("Restart in")
#define _TEST_OPERATIONAL_1_STRING            ("%2d seconds")
#define _TEST_SECOND_STRING                   ("second")
#define _TEST_SECONDS_STRING                  ("seconds")
#define _TEST_DONE                            ("Done!")
/*
String TestMgr::t0[2]    = {"Starting Tests",""},                      // index 0: print, not press, next = 1
       TestMgr::t1[2][2] = {{"Short press to", "Continue"},               // index 1: cycle, wait on press next = 2, or 4
                   {"Long press to", "Cancel"}},
       TestMgr::t2[2]    = {"Do Safety", "Position, ok?"},             // index 2: print, wait on press next = 3, or 4
       TestMgr::t3[2]    = {"Do Release", "Position, ok?"},            // index 3: print, wait on press next = 5, or 4
       TestMgr::t4[2]    = {"Short press to", "Confirme Cancel?"},     // index 4: print, wait on press next = 5, or previous next
       TestMgr::t5[2][2] = {{"Tests complete", ""},                    // index 5: cycle, no press
                   {"Restart in", "%2d seconds"}},
       TestMgr::r0       = "second",
       TestMgr::r1       = "seconds";
*/
/*
const String SubjectAlert      = SubjectPrefix + String("ALERT!!"),
             SubjectReport     = SubjectPrefix + "Status Report",
             SubjectAction     = SubjectPrefix + "Shutter Action Report",
             SubjectBoot       = SubjectPrefix + "System Startup!",
             htmlProlog        = String("Mime-Version: 1.0\n")
                               + String("Content-Type: text/html; charset=\"ISO-8859-1\"\n") 
                               + String("Content-Transfer-Encoding: 7bit\n") 
                               + String("<html><body>\n"),
             htmlPostlog       = String("</body></html>\n"),
             htmlAlarm         = String("<h2>FrogShutters ALARM !</h2>"),
             htmlStatus        = String("<h2>FrogShutters Status Report</h2>"),
             htmlAction        = String("<h1>FrogShutters New Shutter Action Report</h1>"),
             htmlWindSpeedP    = String("<p>Current Wind speed                   : <b>") + wsRS      + String("</b> m/s.</p>"),
             htmlLastNonZeroP  = String("<p>Last Non Zero Wind Speed Measurement : <b>") + lastNZRS  + String("</b> ago.</p>"),
             htmlShutterPosP   = String("<p>Current Shutter Postion              : <b>") + spRS      + String("</b>.</p>");
*/

#endif
