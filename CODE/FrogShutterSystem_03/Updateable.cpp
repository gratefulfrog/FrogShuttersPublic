#include "Updateable.h"

Updateable::Updateable(App &app,String name, long unsigned updatePeriod):_app(app),_name(name),_updatePeriod(updatePeriod) {
  _lastUpdateTime = millis();
  Serial.print("I'm alive!  ");
  _sayMyName();
}

void Updateable::update(unsigned long now){
  if(now-_lastUpdateTime>= _updatePeriod){
    _update(now);
    _lastUpdateTime = now;
  }
}

void Updateable::_sayMyName(String s) const{
  Serial.println(String("Hi, my name is ") + _name + " " + s);
}
