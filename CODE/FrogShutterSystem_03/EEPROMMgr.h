#ifndef EEPROMMGR_H
#define EEPROMMGR_H

#include <Arduino.h>
#include <Wire.h>

#include "App.h"
#include "Tools.h"
#include "Config.h"


class EEPROMMgr{
  protected:
    const uint8_t _deviceI2CAddress = EEPROM_I2D_ADDRESS;
    const unsigned int _shutterPositionAddress = 0,
                       _releaseThresholdAddress = _shutterPositionAddress + sizeof(byte),
                       _safetyThresholdAddress  = _releaseThresholdAddress + sizeof(float);
    const unsigned int _size;
  public:
    EEPROMMgr();
    void init(byte initVal = 0) const;
    byte getByte(unsigned int EEPROMAddress)  const;
    void getBytes(unsigned int EEPROMAddress, byte bVec[], byte nbBytes) const;
    void updateByte(unsigned int EEPROMAddress, byte outGoing) const;
    void updateBytes(unsigned int EEPROMAddress, byte bVec[], byte nbBytes) const;
    void writeBytes(unsigned int EEPROMAddress, byte outGoing[], byte nbBytes) const;
};   

class ShutterStorageMgr: protected EEPROMMgr{
  public:
    ShutterStorageMgr();
    void updateShutterPos(byte pos) const;
    void updateReleaseThreshold(float releaseThreshold) const; 
    void updateSafetyThreshold(float safetyThreshold) const; 
    void updateIndexedThreshold(byte index, float threshold) const; 
    byte  getShutterPosition() const;
    float getReleaseThreshold() const; 
    float getSafetyThreshold() const;
    float getIndexedThreshold(byte index) const; 
    void init() const;
};


/*
//#define USE_EXT_EEPROM
//#define USE_BOARD_EEPROM
#define USE_WIRE


#ifdef USE_BOARD_EEPROM
#include <EEPROM.h>
#endif
/*
#ifdef USE_EXT_EEOM
#include <extEEPROM.h>
#endif 


class EEPROMMgr{
  protected:
    const uint8_t _deviceI2CAddress = EEPROM_I2D_ADDRESS;
    const unsigned int _shutterPositionAddress = 0,
                       _releaseThresholdAddress = _shutterPositionAddress + sizeof(byte),
                       _safetyThresholdAddress  = _releaseThresholdAddress + sizeof(float);
    const unsigned int _size;
    void _update(unsigned int EEPROMAddress, byte bVec[], byte nbBytes) const;
    void _getBytes(unsigned int EEPROMAddress, byte bVec[], byte nbBytes) const;
  public:
    EEPROMMgr();
    void init();
    void updateShutterPos(byte pos) const;
    void updateReleaseThreshold(float releaseThreshold) const; 
    void updateSafetyThreshold(float safetyThreshold) const; 
    void updateIndexedThreshold(byte index, float threshold) const; 
    byte  getShutterPosition() const;
    float getReleaseThreshold() const; 
    float getSafetyThreshold() const;
    float getIndexedThreshold(byte index) const;
};   
*/
#endif
