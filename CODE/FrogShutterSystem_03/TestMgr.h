#ifndef TESTMGR_H
#define TESTMGR_H

#include "Config.h"
#include "Updateable.h"
#include "TimedPB.h"

class TestMgr;

typedef void (TestMgr::*cfPtr)();

class TestMgr: public Updateable{
 protected:
  static String t0[2],
    t1[2][2],
    t2[2],
    t3[2],
    t4[2],
    t5[2][2],
    r0,
    r1,
    done,
    sStringVec[][2],
    dStringVec[][2][2];

  static const int _nbTestFuncs = 6;
    
  protected:
    virtual void _update(long unsigned now);

    void _setShutterPosition(byte postionType);
    bool _getPress(bool &ret);

    // test funcs
    void _f1();
    void _f2();  // ind: 1 or 5
    void _f3();
    void _f4();
    void _f5();
    void _runTests();

    static const cfPtr _fVec[_nbTestFuncs];

    TimedPB *_tpb;
    int _prevInd = 0,
        _currentFuncIndex =0;
    
    //byte _testPhase = 0;
    //const byte _nbTestPhases = 3;
    
  public:
    TestMgr(App &_app,String name = "The Test Manager", long unsigned updatePeriod = TEST_MGR_POLL_PERIOD);
    //void cycleTestPhase();

  
};

#endif
