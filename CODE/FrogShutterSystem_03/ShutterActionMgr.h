#ifndef SHUTTER_ACTION_MGR_H
#define SHUTTER_ACTION_MGR_H

#include "Config.h"
#include "Updateable.h"

class ShutterActionMgr;
typedef bool (ShutterActionMgr::*conditionFunctionPtr)(unsigned long now) const;
typedef void (ShutterActionMgr::*actionFunctionPtr)(unsigned long now);

struct conditionActionPair {
  conditionFunctionPtr cfPtr;
  actionFunctionPtr    afPtr;
};


class ShutterActionMgr: public Updateable{
  // take actions on shutter position depending on truth table: shutter position to safety, or realease or no action 
  protected:
    virtual void _update(long unsigned now);// defined here!
    void _shutterActionMgrFunction();

    static const long unsigned _minTimeBelowReleaseThresholdForAction = SHUTTER_ACTION_MGR_MIN_TIME_BELOW_RELEASE_THRESHOLD,
                               _yokisPulseTime                        = SHUTTER_ACTION_MGR_YOKIS_PIN_TIME,
                               _yokisNoPulseDelay                     = SHUTTER_ACTION_MGR_YOKIS_NO_PULSE_DELAY;
    static const byte _nbConditions =8;
    static const conditionActionPair _caPairVec[_nbConditions];
    
    bool _postPulseHoldCondition(unsigned long now) const,
         _postPulseUnHoldCondition(unsigned long now) const,
         _unPulseCondition(unsigned long now) const,
         _morePulseCondition(unsigned long now) const,
         _condition0(unsigned long now) const,
         _condition1(unsigned long now) const,
         _condition2(unsigned long now) const,
         _condition3(unsigned long now) const,
         _pulsing = false,
         _noActionDelay = false;

    void _safetyAction(unsigned long now),
         _resetSafetyAction(unsigned long now),
         _releaseAction(unsigned long now),
         _resetThresholdCrossingTimeAction(unsigned long now),
         _noAction(unsigned long now),
         _unPulseAction(unsigned long now),
         _unHoldAction(unsigned long now);

    unsigned long _releaseThresholdPassingTime,
                  _yokisPulseStartTime;

  public:
    ShutterActionMgr(App &_app,String name = "The Shutter Action Manager", long unsigned updatePeriod = SHUTTER_ACTION_MGR_POLL_PERIOD);
    bool isPulsing() const;
};

#endif
