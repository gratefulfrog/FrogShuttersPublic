#ifndef ALARMMGR_H
#define ALARMMGR_H

#include "Config.h"
#include "Updateable.h"

class AlarmMgr: public Updateable{
  // give alarm if needed
  protected:
    virtual void _update(long unsigned now); // defined here!

  public:
    AlarmMgr(App &_app,
	     String name = "The Alarm Manager",
	     long unsigned updatePeriod = ALARM_MGR_POLL_PERIOD);
};

#endif
