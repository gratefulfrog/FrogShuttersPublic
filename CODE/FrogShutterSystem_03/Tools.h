#ifndef TOOLS_H
#define TOOLS_H

#include <Arduino.h>


extern void valueToBytes(const void *inVal , byte outVec[], byte size);
extern void bytesToValue(const byte inbVec[],void *outVal, byte size);
extern String millisToHHMMSS(float ms);  // arg is float so that it is automatically type cast ready for division
extern void spaceTimeString (String &timeString, int spaceAvailable);  // timeString referenced is updated!


#endif
