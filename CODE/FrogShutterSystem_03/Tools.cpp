#include "Tools.h"

void valueToBytes(const void *inVal , byte outVec[], byte size){
  byte *bPtr = (byte*)(inVal);
  for (byte i=0;i<size;i++){
    outVec[i]=*(bPtr+i);   
  }
}

void bytesToValue(const byte inbVec[],void *outVal, byte size){
  byte *bPtr = (byte*)outVal;
  for (byte i=0;i<size;i++){ 
    *(bPtr+i) = inbVec[i];
  }
}

String millisToHHMMSS(float ms){
  static const char format[] = "%02u:%02u:%02u";
  // arg is float so that it is automatically type cast ready for division
  unsigned ss =  int(round(floor(ms /(1000.0)))) % 60,
           mm =  int(round(floor(ms /(1000.0*60)))) % 60,
           hh =  int(floor(round(ms /(1000.0*60*60))));
   char buff[9];
   sprintf(buff,format,hh,mm,ss);
   return String(buff);
}

void spaceTimeString (String &timeString, int spaceAvailable){  // timeString referenced is updated!
  while(timeString.length() < spaceAvailable){
    timeString = " " + timeString;
  }
}
