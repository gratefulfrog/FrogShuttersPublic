#ifndef ARDUINO_SECRETS_H
#define ARDUINO_SECRETS_H

#include "/mnt/EXT4_160GB/FrogShutters/SecretCode/bob_arduino_secrets.h"

#define SECRET_WIFI_SSID                B_SECRET_SSID                   // ("My home Wifi network")
#define SECRET_WIFI_PASS                B_SECRET_PASS                   // ("My home wifi password")

#define SECRET_SEND_NAME                B_SECRET_SEND_NAME              // ("Somebody SomeLatName") // sender's name                                       
#define SECRET_SEND_ACCOUNT             B_SECRET_SEND_ACCOUNT           // ("somebody@gmail.com")   // sender's gmail account
#define SECRET_SEND_ACCOUNT_PASSWORD    B_SECRET_SEND_ACCOUNT_PASSWORD  // ("wxyzabcdlmnorstu")     // sender's gmail app aspecific password
#define SECRET_DOMAIN                   B_SECRET_DOMAIN                 // ("domain-abc.org")       // sender's domain

#endif
