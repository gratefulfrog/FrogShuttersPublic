#ifndef CONFIG_H
#define CONFIG_H

#include <Arduino.h>

////////////////////////////////////////////
//////////// Spoken Language ///////////////
////////////////////////////////////////////
#define USE_ENGLISH       (0)
#define USE_FRENCH        (1)
#define SPOKEN_LANGUAGE   USE_FRENCH // USE_ENGLISH   

#if (SPOKEN_LANGUAGE == USE_FRENCH)
#include "vocab.fr.h"
#else
#include "vocab.en.h"
#endif

/*
#define _STARTUP_MSG(lang)     ((lang == USE_FRENCH) ? ("Mise En Route !!") : (" Starting Up !! "))
#define _TRUE_CHAR(lang)       ((lang == USE_FRENCH) ? ('V') : ('T'))
#define _FALSE_CHAR(lang)      ('F')
#define _LCD_EMAIL_MSG_L0(lang)    ((lang == USE_FRENCH) ?   ("Envoi d'email...") : ("Sending email..."))
#define _LCD_EMAIL_MSG_L1(lang)    ((lang == USE_FRENCH) ?   ("Maintenant !") : ("Now !"))
#define _LCD_ALARM_MSG_L0(lang)    ((lang == USE_FRENCH) ?   ("  A L E R T E ! ") : ("  A L A R M !  "))
*/

#define STARTUP_MSG_STRING       (String(_STARTUP_MSG))
#define TRUE_CHAR_STRING         (String(_TRUE_CHAR))
#define FALSE_CHAR_STRING        (String(_FALSE_CHAR))
#define WINDSPEED_UNIT_STRING    (String((" ms ")))
#define LCD_EMAIL_MSG_L0         (String(_LCD_EMAIL_MSG_L0))
#define LCD_EMAIL_MSG_L1         (String(_LCD_EMAIL_MSG_L1))
#define LCD_ALARM_MSG_L0         (String(_LCD_ALARM_MSG_L0))
#define LCD_ALARM_MSG_L1         (String(_LCD_ALARM_MSG_L0))

// from App.cpp
#define UNKNOWN_STRING (String(_UNKNOWN))
#define SAFETY_STRING  (String(_SAFETY))
#define RELEASING_STRING (String(_RELEASING))
#define RELEASED_STRING (String(_RELEASED))

// MessageFormats.cpp
#define SUBJECT_ALERT_STRING                  _SUBJECT_ALERT_STRING
#define SUBJECT_REPORT_STRING                 _SUBJECT_REPORT_STRING
#define SUBJECT_ACTION_STRING                 _SUBJECT_ACTION_STRING
#define SUBJECT_BOOT_STRING                   _SUBJECT_BOOT_STRING
#define HTML_ALARM_STRING                     _HTML_ALARM_STRING
#define HTML_STATUS_STRING                    _HTML_STATUS_STRING
#define HTML_ACTION_STRING                    _HTML_ACTION_STRING
#define HTML_WINDSPEED_STRING                 _HTML_WINDSPEED_STRING
#define HTML_MAX_WINDSPEED_STRING             _HTML_MAX_WINDSPEED_STRING
#define HTML_LAST_NON_ZERO_STRING             _HTML_LAST_NON_ZERO_STRING
#define HTML_SHUTTER_POSTION_STRING           _HTML_SHUTTER_POSTION_STRING
#define HTML_SAFETY_THRESHOLD_STRING          _HTML_SAFETY_THRESHOLD_STRING    
#define HTML_RELEASE_THRESHOLD_STRING         _HTML_RELEASE_THRESHOLD_STRING

// TestMgr.cpp
#define TEST_STARTUP_0_STRING                 _TEST_STARTUP_0_STRING
#define TEST_STARTUP_1_STRING                 _TEST_STARTUP_1_STRING
#define TEST_SHORT_PRESS_STRING               _TEST_SHORT_PRESS_STRING         
#define TEST_CONTINUE_STRING                  _TEST_CONTINUE_STRING            
#define TEST_LONG_PRESS_STRING		            _TEST_LONG_PRESS_STRING          
#define TEST_CANCEL_STRING		                _TEST_CANCEL_STRING              
#define TEST_POSITION_SAFETY_0_STRING	        _TEST_POSITION_SAFETY_0_STRING   
#define TEST_POSITION_SAFETY_1_STRING	        _TEST_POSITION_SAFETY_1_STRING   
#define TEST_POSITION_RELEASED_0_STRING	      _TEST_POSITION_RELEASED_0_STRING 
#define TEST_POSITION_RELEASED_1_STRING	      _TEST_POSITION_RELEASED_1_STRING 
#define TEST_CONFIRM_CANCEL_STRING	          _TEST_CONFIRM_CANCEL_STRING      
#define TEST_COMPLETE_STRING		              _TEST_COMPLETE_STRING            
#define TEST_OPERATIONAL_0_STRING	            _TEST_OPERATIONAL_0_STRING       
#define TEST_OPERATIONAL_1_STRING	            _TEST_OPERATIONAL_1_STRING       
#define TEST_SECOND_STRING 		                _TEST_SECOND_STRING              
#define TEST_SECONDS_STRING                   _TEST_SECONDS_STRING             
#define TEST_DONE                             _TEST_DONE

////////////////////////////////////////////
//////////// pin assignments ///////////////
////////////////////////////////////////////
// 14 pins are used
//  6 interupts are used
// byte values
// updated for version 2!

#define YOKIS_CLOSE_PIN        (A0)   // OUTPUT
#define WIND_SENSOR_PIN        (A1)   // INPUT interrupt
#define YOKIS_OPEN_PIN         (A2)   // OUTPUT
#define BUZZER_PIN             (A3)   // OUTPUT
//#define                      (A4)   // not connected
//#define                      (A5)   // IDC connector reserve
#define LED_OPEN_PIN           (A6)   // OUTPUT
#define KEY_RIGHT_PIN           (0)   // INPUT interrupt
#define DIR_RIGHT_PIN           (1)   // INPUT
#define LED_CLOSE_PIN           (2)   // OUTPUT
#define LED_PULSE_PIN           (3)   // OUTPUT
#define SIG_RIGHT_PIN           (4)   // INPUT interrupt
#define PUSHBUTTON_PIN          (5)   // INPUT interrupt
#define SIG_LEFT_PIN            (6)   // INPUT interrupt 
#define DIR_LEFT_PIN            (7)   // INPUT
#define KEY_LEFT_PIN            (8)   // INPUT interrupt 
//#define                       (9)   // IDC connector reserve
//#define                      (10)   // not connected
//#define                      (11)   // SDA
//#define                      (12)   // SCL
//#define                      (13)   // not connected
//#define                      (14)   // not connected


////////////////////////////////////////////
//////////// Threshold Values //////////////
////////////////////////////////////////////
// values in meters/second
// float values
//
#define RELEASE_THRESHOLD_DEFAULT    (7.0)  // 25 kmh
#define SAFETY_THRESHOLD_DEFAULT    (12.0)  // 43 kmh

////////////////////////////////////////////
//////////// Timeout Values //////////////
////////////////////////////////////////////
// values in milliseconds
// long unsigned values
//
// computational constants

#define MILLIS_PER_SEC                                      (1000)    // intentionally not typed!
#define ZERO_SECONDS                           ((unsigned long)(0))
#define ONE_SECOND                          ((unsigned long)(1000))
#define TEN_SECONDS                ((unsigned long)(10*ONE_SECOND))
#define THIRTY_SECONDS             ((unsigned long)(30*ONE_SECOND))
#define ONE_MINUTE                 ((unsigned long)(60*ONE_SECOND))
#define THREE_MINUTES               ((unsigned long)(3*ONE_MINUTE))
#define TEN_MINUTES                ((unsigned long)(10*ONE_MINUTE))
#define ONE_HOUR                   ((unsigned long)(60*ONE_MINUTE))
#define TWO_HOURS                 ((unsigned long)(120*ONE_MINUTE))
#define SIX_HOURS                     ((unsigned long)(6*ONE_HOUR))
#define TWELVE_HOURS                 ((unsigned long)(12*ONE_HOUR))
#define ONE_DAY                      ((unsigned long)(24*ONE_HOUR))

// domain parameter values
#define LCD_BACKLIGHT_TIMEOUT              (2*(THREE_MINUTES))
#define LCD_MGR_POLL_PERIOD                (ZERO_SECONDS)
#define WIND_SENSOR_POLL_PERIOD            (TEN_SECONDS)
#define TEST_MGR_POLL_PERIOD               (ZERO_SECONDS)
#define THRESHOLD_MGR_POLL_PERIOD          (ZERO_SECONDS)
#define COMMS_MGR_POLL_PERIOD              (TWELVE_HOURS) // (TEN_MINUTES)  // (SIX_HOURS)
#define ALARM_MGR_POLL_PERIOD              (SIX_HOURS)    // (TEN_SECONDS)  // (SIX_HOURS)
#define ALARM_MGR_MAX_NON_ZERO_WS_INTERVAL (TWELVE_HOURS) // (ONE_MINUTE)   // (TWELVE_HOURS) 
// 2024 12 22 update
// WindSpeedMgr.cpp
#define WINDSPEED_MGR_ZERO_WIND_SPEED      (0.05)  // meters/sec was 0.1 but that was too high for ZERO
#define ALARM_NB_FLASHES                   (10)     // the alarm leds and buzzer flash this number of times
/******************  NOTE THE NEXT LINE IS  A TEST VALUE ONLY ********************/
#define SHUTTER_ACTION_MGR_MIN_TIME_BELOW_RELEASE_THRESHOLD     (TWO_HOURS) // (ONE_MINUTE) // test value! (TWO_HOURS)  // real value!
#define SHUTTER_ACTION_MGR_YOKIS_PIN_TIME                       ((unsigned long)(1500)) // time to hold contact for YOKIS
#define SHUTTER_ACTION_MGR_YOKIS_NO_PULSE_DELAY                 (THIRTY_SECONDS)
#define SHUTTER_ACTION_MGR_POLL_PERIOD                          (ZERO_SECONDS)
 
// technical parameter values
#define INTERRUPT_DEBOUNCE_DELAY               ((unsigned long)(5))  // millisecond
#define WIND_SENSOR_PULSES_PER_REVOLUTION      (2)
#define TIMED_PB_MAX_SHORT_TIME                ((unsigned long)(600)) //milliseconds, was 400 but bizarre behaviour with front panel

////////////////////////////////////////////
/////////////// EEPROM /////////////////////
////////////////////////////////////////////
#define EEPROM_I2D_ADDRESS                    (0x50)  // A0,A1,A2 tied to GND
#define EEPROM_24LC256_SIZE                   (32767) // size in bytes, i.e 2^15-1 Bytes
#define EEPROM_SHUTTER_POSITION_ADDRESS       (0x00)  // one byte 
#define EEPROM_RELEASE_THRESHOLD_ADDRESS      (0x01)  // one byte later
#define EEPROM_SAFETY_THRESHOLD_ADDRESS       (EEPROM_RELEASE_THRESHOLD_ADDRESS +sizeof(float))

////////////////////////////////////////////
///////////////   LCD  /////////////////////
////////////////////////////////////////////
#define LCD_I2C_ADDRESS                       (0x27) 
#define LCD_NB_COLS                           (16)
#define LCD_NB_ROWS                           (2)

////////////////////////////////////////////
/////////   COMMS Mgr  /////////////////////
////////////////////////////////////////////
#define COMMS_EMAIL_ON_SHUTTER_POSITION_CHANGE      (1)
#define COMMS_EMAIL_ON_THRESHOLD_CHANGE             (1)
#define COMMS_EMAIL_ON_WIND_SENSOR_ALARM            (1)
#define COMMS_EMAIL_ON_TIMED_EMAIL_TIMEOUT          (1)
#define COMMS_EMAIL_TIMEOUT                         (SIX_HOURS)

////////////////////////////////////////////
/////////    ALARM     /////////////////////
////////////////////////////////////////////
#define ALARM_FLASH_PAUSE                         (ONE_SECOND)
#define ALARM_BUZZER_FREQ                         (440)


#endif
