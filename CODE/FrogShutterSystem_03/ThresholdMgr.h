#ifndef THRESHOLD_MGR_H
#define THRESHOLD_MGR_H

#include "Config.h"
#include "Updateable.h"
#include "HWDebouncedKeyedRotaryEncoder.h"

class ThresholdMgr:  public Updateable{
   // use all the data to update LCD and send email if necessary.
  protected:
    virtual void _update(long unsigned now);  // defined here!
    void _thresholdMgrFunction();
    
    HWDKRE *_encoderPtrL,
           *_encoderPtrR;

  public:
    ThresholdMgr(App &_app,String name = "The Threshold Manager", long unsigned updatePeriod = THRESHOLD_MGR_POLL_PERIOD);
};

#endif
