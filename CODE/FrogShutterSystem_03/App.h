#ifndef APP_H
#define APP_H
#include <Arduino.h>
#include <Wire.h>

#include "Config.h"
#include "LCDMgr.h"
#include "WindSpeedMgr.h"
#include "TestMgr.h"
#include "ThresholdMgr.h"
#include "ShutterActionMgr.h"
#include "CommsMgr.h"
#include "AlarmMgr.h"

// only define this to initialize a brand new EEPROM !
//#define EEPROM_INIT

struct thresholdTimeStruct{
  float threshold;
  unsigned long time;
};

class ShutterStorageMgr;
class LCDMgr;
class WindSpeedMgr;

class App{    
  public:
    static const byte   releaseThresholdIndex = 0,
                        safetyThresholdIndex  = 1,
                        nbVecElts             = 2;
    static const byte   shutterPositionUnknown  = 0,
                        shutterPostionSafety    = 1,
                        shutterPostionReleasing = 2,
                        shutterPositionRelease  = 3; 
    static const String shutterPostionNameVec[] ;
    
  protected:
    byte                _shutterPosition;
    thresholdTimeStruct _thresholdTimeStructVec[nbVecElts];

  public:
    // pins
    const byte _yokisClosePIN  = YOKIS_CLOSE_PIN,  // output
               _pushButtonPin  = PUSHBUTTON_PIN,   // interrupt PULL UP
               _windSensorPin  = WIND_SENSOR_PIN,  // interrupt PULL UP
               _yokisOpenPin   = YOKIS_OPEN_PIN,   // output
               _buzzerPin      = BUZZER_PIN,       // output
               _ledOpenPin     = LED_OPEN_PIN,     // output
               _ledClosePin    = LED_CLOSE_PIN,    // output
               _signalRightPin = SIG_RIGHT_PIN,    // interrupt PULL UP
               _keyRightPin    = KEY_RIGHT_PIN,    // interrupt PULL UP
               _dirRightPin    = DIR_RIGHT_PIN,    // input no pull
               _dirLeftPin     = DIR_LEFT_PIN,     // input no pull
               _keyLeftPin     = KEY_LEFT_PIN,     // interrupt PULL UP
               _signalLeftPin  = SIG_LEFT_PIN,     // interrupt PULL UP
               _ledPulsePin    = LED_PULSE_PIN;   // output
               
  protected:
    const byte *_outputPinAdrVec[7]      = {&_yokisClosePIN,
                                            &_yokisOpenPin,
                                            &_buzzerPin,
                                            &_ledOpenPin,
                                            &_ledClosePin,
                                            &_ledPulsePin,
                                            NULL},
               *_inputNoPullPinAdrVec[9] = { &_dirRightPin,
                                            &_dirLeftPin,
					                                  &_pushButtonPin,
                                            &_windSensorPin,
                                            &_signalRightPin,
                                            &_keyRightPin,
                                            &_keyLeftPin,
                                            &_signalLeftPin,
                                            NULL},
               *_inputPullUpPinAdrVec[1] = {NULL};

    Updateable **_updateableMgrAdrVec; 

                
    TestMgr          *_testMgr;            // 0
    WindSpeedMgr     *_windSpeedMgr;       // 1
    ShutterActionMgr *_shutterActionMgr;   // 2
    ThresholdMgr     *_thresholdMgr;       // 3
    AlarmMgr         *_alarmMgr;           // 4
    CommsMgr         *_commsMgr;           // 5
    LCDMgr           *_lcdMgr;             // 6
    
    const byte _nbMgrs = 7;
    
    ShutterStorageMgr *_ssMgr;  // not an updateable!
    
    void _initializeState();
    void _initializePins();  
    void _initializeMgrs();
    void _doPulse() const;
    
  public:
    String        getShutterPositionName() const;
    byte          getShutterPosition() const;
    void          setShutterPosition(byte pos = shutterPositionUnknown);
    float         getReleaseThresholdValue() const;
    void          setReleaseThresholdValue(float) ;
    unsigned long getReleaseThresholdTime() const;
    float         getSafetyThresholdValue() const;
    void          setSafetyThresholdValue(float) ;
    unsigned long getSafetyThresholdTime() const;
    unsigned long getLastNonZeroWSInterval() const;
    float         getCurrentWindSpeed(bool useCurrent = true) const;
    float         getMaxWindSpeed() const;
    void          resetMaxWindSpeed();
    App();
    void mainLoop(); 
    void updateLCD(bool forceOn = false);
    void lcdEmailMsg(bool yes);
    void lcdDisplayMsg(String line0, String line1,bool forceOn = true);
    void lcdDisplayMsg(String msg, bool forceOn = true);
    void giveAlarm();                              
};

#endif
