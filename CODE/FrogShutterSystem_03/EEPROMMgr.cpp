#include "EEPROMMgr.h"
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////


EEPROMMgr::EEPROMMgr():
  _size(EEPROM_24LC256_SIZE){
}

void EEPROMMgr::getBytes(unsigned int EEPROMAddress, byte bVec[], byte nbBytes) const{
  //Serial.println("getBytes: " + String(EEPROMAddress) + " : nb bytes : " + String(nbBytes));
  Wire.beginTransmission(_deviceI2CAddress);
  Wire.write((byte)((EEPROMAddress) >> 8));   // MSB
  Wire.write((byte)((EEPROMAddress) & 0xFF)); // LSB
  Wire.endTransmission();
 
  Wire.requestFrom(_deviceI2CAddress,nbBytes);
  byte i = 0;
  while (Wire.available()&& i<nbBytes) {
    bVec[i++] = Wire.read();
  }  
}

byte EEPROMMgr::getByte(unsigned int EEPROMAddress) const{
  byte res[1];
  getBytes(EEPROMAddress, res, 1);
  return res[0];
}

void EEPROMMgr::updateBytes(unsigned int EEPROMAddress, byte bVec[], byte nbBytes) const{
  // does checking but is slow!
  for (unsigned int i= 0; i< nbBytes; i++){
    updateByte(EEPROMAddress+i,bVec[i]);
  }
}
void EEPROMMgr::updateByte(unsigned int EEPROMAddress, byte outGoing) const{
  byte currVal = getByte(EEPROMAddress);
  //Serial.println("Current val: " + String(currVal));
  if (currVal != outGoing){
    //Serial.println("Writing: " + String(outGoing));
    Wire.beginTransmission(_deviceI2CAddress);
    Wire.write((byte)((EEPROMAddress) >> 8));   // MSB
    Wire.write((byte)((EEPROMAddress) & 0xFF)); // LSB
    Wire.write(outGoing);
    Wire.endTransmission();
    delay(6);
  }
  else{
    //Serial.println("NOT Writing...");
  }
  //currVal = getByte(EEPROMAddress);
  //Serial.println("value read back: " + String(currVal));
}

void EEPROMMgr::writeBytes(unsigned int EEPROMAddress, byte outGoingVec[], byte nbBytes) const{
  Wire.beginTransmission(_deviceI2CAddress);
  Wire.write((byte)((EEPROMAddress) >> 8));   // MSB
  Wire.write((byte)((EEPROMAddress) & 0xFF)); // LSB 
  Wire.write(outGoingVec,nbBytes); 
  Wire.endTransmission();
  delay(6);
}

void EEPROMMgr::init(byte initVal) const{
  //Serial.println("INIT EEPROM:  Size: " + String(_size));
  // this is the paged version with Wire..
  const unsigned int pageSize = 64; // bytes
  unsigned int pageAddress    = 0;
  unsigned int count          = 0;  // for display only

  byte outVec[pageSize];
  // init outvec
  for (byte i = 0;i<pageSize;i++){
    outVec[i] = initVal;
  }
  
  while(pageAddress< _size){
    Wire.beginTransmission(_deviceI2CAddress);
    Wire.write((byte)((pageAddress) >> 8));   // MSB
    Wire.write((byte)((pageAddress) & 0xFF)); // LSB
    Wire.write(outVec,pageSize);
    Wire.endTransmission(); 
    //Serial.print("Page Address: " + String(pageAddress+pageSize) + "  : ");
    //Serial.println("Wrote page: " + String(++count) + "/" + String((_size+1)/pageSize));
    delay(6);
    pageAddress +=pageSize ;
  }
}

void ShutterStorageMgr::updateShutterPos(byte pos) const{
  //Serial.println("Byte going out: " + String(pos));
  updateByte(_shutterPositionAddress, pos);
}

void ShutterStorageMgr::updateReleaseThreshold(float releaseThreshold) const{
  byte bVec[sizeof(float)];
  valueToBytes(&releaseThreshold , bVec, sizeof(float));
  //Serial.println("Bytes going out: " + String(releaseThreshold));
  //showBytes(bVec, sizeof(float));
  updateBytes(_releaseThresholdAddress, bVec, sizeof(float));
} 

void ShutterStorageMgr::updateSafetyThreshold(float safetyThreshold) const {
  byte bVec[sizeof(float)];
  valueToBytes(&safetyThreshold , bVec, sizeof(float));
  //Serial.println("Bytes going out: " + String(safetyThreshold));
  //showBytes(bVec,  sizeof(float));
  updateBytes(_safetyThresholdAddress, bVec, sizeof(float));
} 

void ShutterStorageMgr::updateIndexedThreshold(byte index, float threshold) const{
  if (index == App::releaseThresholdIndex){
    updateReleaseThreshold(threshold);
  }
  else{
    updateSafetyThreshold(threshold);
  }
}

byte  ShutterStorageMgr::getShutterPosition() const{
  return getByte(_shutterPositionAddress) ;
}

float ShutterStorageMgr::getReleaseThreshold() const{
  byte bVec[sizeof(float)];
  getBytes(_releaseThresholdAddress, bVec, sizeof(float)) ;
  float res;
  bytesToValue(bVec, &res, sizeof(float));  
  //Serial.println("Calling epm.getBytes");  
  //Serial.println("Bytes gotten");
  //showBytes(bVec, sizeof(float));
  return res;
}

float ShutterStorageMgr::getSafetyThreshold() const{
  byte bVec[sizeof(float)];
  getBytes(_safetyThresholdAddress, bVec, sizeof(float)) ;
  float res;
  bytesToValue(bVec, &res, sizeof(float)); 
  //Serial.println("Calling epm.getBytes");  
  //Serial.println("Bytes gotten");
  //showBytes(bVec, sizeof(float));
  return res;
}
float ShutterStorageMgr::getIndexedThreshold(byte index) const{
  float res;
  if (index == App::releaseThresholdIndex){
    res = getReleaseThreshold();
  }
  else{
    res =getSafetyThreshold();
  }
  return res;
}
void ShutterStorageMgr::init() const{
  EEPROMMgr::init();
}

ShutterStorageMgr::ShutterStorageMgr():EEPROMMgr(){
}
