#include "WindSpeedMgr.h"

// inclusion here to avoid conflicting header files
#include "App.h"

void WindSpeedMgr::_update(long unsigned now){
  _sayMyName("and I'm updating!");
  _windSpeedMgrFunction(now);
}
 
void WindSpeedMgr::_windSpeedMgrFunction(unsigned long now){
  float newWindSpeed = _ews->getWindSpeed(now);
  _windSpeedVec[lastWSIndex] = _windSpeedVec[currentWSIndex];
  _windSpeedVec[currentWSIndex] = newWindSpeed;
  _maxWindSpeed = max(_maxWindSpeed,_windSpeedVec[currentWSIndex]);  

  if (_windSpeedVec[currentWSIndex]>=WINDSPEED_MGR_ZERO_WIND_SPEED /*0.1*/){  
    _lastNonZeroWSTime = now;
  }
  _app.updateLCD();
}

WindSpeedMgr::WindSpeedMgr(App &_app,String name, long unsigned updatePeriod):Updateable(_app, name, updatePeriod){
  _ews = new EltakoWindSensor();
  resetMaxWindSpeed();
}

unsigned long WindSpeedMgr::getLastNonZeroWSTime() const{
  return _lastNonZeroWSTime;
}
unsigned long WindSpeedMgr::getLastNonZeroWSInterval(unsigned long now) const{
  return now - _lastNonZeroWSTime;
}
   
float  WindSpeedMgr::getWindSpeed(byte index) const{
  return _windSpeedVec[index];
}
float  WindSpeedMgr::getMaxWindSpeed() const{
  return _maxWindSpeed;
}
void   WindSpeedMgr::resetMaxWindSpeed(){
  _maxWindSpeed = 0.0;
}
