#ifndef UPDATEABLE_H
#define UPDATEABLE_H

#include <Arduino.h>

class App;

class Updateable{
  protected:
    App            &_app;
    const String    _name;
    long unsigned   _updatePeriod;       // milliseconds
    long unsigned   _lastUpdateTime = 0;  // milliseconds

    virtual void _update(long unsigned now) = 0;  // declared like this means it must be defined in each child class
    void _sayMyName(String s = "") const;

  public:
    Updateable(App &_app,String name, long unsigned updatePeriod);
    void update(unsigned long now);
};

#endif
