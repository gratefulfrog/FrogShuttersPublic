#ifndef MESSAGE_FORMATS_H
#define MESSAGE_FORMATS_H
#include <Arduino.h>
#include "Config.h"

extern const String SubjectAlert,
                    SubjectReport,
                    SubjectAction,
                    SubjectBoot,

                    MsgAlertFormat,
                    MsgReportFormat,
                    MsgActionFormat,

                    wsRS,
                    mwsRS,
                    lastNZRS, 
                    spRS,
                    stRS,
                    rtRS;
#endif
