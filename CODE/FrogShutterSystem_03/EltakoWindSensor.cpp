#include "EltakoWindSensor.h"

// Static definition
EltakoWindSensor *EltakoWindSensor::_thisPtr;

// static class method
void EltakoWindSensor::pulseCountISR(){
  _thisPtr->_count++;
}

// instance constructor
EltakoWindSensor::EltakoWindSensor(byte pin):
  _count(0),
  _lastCount(0), 
  _lastCountStartTime(millis()){
    EltakoWindSensor::_thisPtr = this;
    pinMode(pin,INPUT);  // pull up is part of the debounce cirucit; do not pull up here!!
    attachInterrupt(digitalPinToInterrupt(pin),EltakoWindSensor::pulseCountISR, RISING);
}

float EltakoWindSensor::getWindSpeed(long unsigned now){
  // returns wind speed in meters/sec and updates internal state
  long unsigned intervalMilliSeconds = now - _lastCountStartTime,
                currentCount = _count,
                counted = currentCount - _lastCount;
  _lastCount = currentCount;
  _lastCountStartTime = now;
  return float(counted)*MILLIS_PER_SEC/(WIND_SENSOR_PULSES_PER_REVOLUTION *(float)intervalMilliSeconds);
}
