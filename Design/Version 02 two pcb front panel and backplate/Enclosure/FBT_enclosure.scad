$fn=100;

//dxfFileName = "DXF/Enclosure.dxf";
//dxfFileName = "DXF/Enclosure_back_position.dxf";
dxfFileName = "DXF/Enclosure_front_position.dxf";
dxfStrapFilename = "DXF/Enclosure_yokis_strap_position.dxf";

fpLayer            = "2_FPanel";
fpExtensionLayer   = "FrontExtension";
tpLayer            = "0_TPanel";
bpLayer            = "1_BPanel";
bpTrimLayer        = "BP_Trimmer";
bpFootTrimmerLayer = "BP_foot_Trimmer";
bpExtensionLayer   = "BP_Extension";
yokiStrapLayer     = "YokisStrapProfile";

sideThickness      = 3;
pcbDim             = 82;
m3d                = 3.4;
pcbThickness       = 2;
lipThickness       = pcbThickness;
upperLipThickness  = lipThickness + 1;
mountingHoleOffset = 3.3;
DY_0               = 42.36;
DYCorrection       = 0; //5 from 2020/02/13__wide_00;
DY                 = DY_0 + DYCorrection ;
DX_0               = 9.27;  // changed!
DXCorrection       = 0; 
DX                 = DX_0 + DXCorrection ;

module slicer(z){
  translate([DX,DY,z])
    rotate([-90,0,30])
        cylinder(h= 60,d=m3d,center=true);
}
%slicer(0);    
    
module fp(){
    difference(){
      union(){
        fpExtension();
        linear_extrude(pcbDim)
          import(dxfFileName,layer=fpLayer);
      }
      union(){
        slicer(3.3);
        slicer (82-3.3);
      }
    }
}
//fp();

module fpExtension(){
  translate([0,0,- sideThickness])
    linear_extrude(pcbDim+2*sideThickness)
      import(dxfFileName,layer=fpExtensionLayer);
}

 //fpExtension();

pts = [[0,0],
       [6,0],
       [6,6],
       [0,6]];

module tab(cutout=true){
  linear_extrude(upperLipThickness)
    translate([sideThickness,0,0])
    difference(){
      polygon(pts);
      if(cutout){
        translate([mountingHoleOffset,mountingHoleOffset])
          circle(d=m3d);
      }
    }
  }
//tab();
  
buzzPts = [[6,0],
           [0,-9],
           [0,-12],
           [6,-21]];
  
module buzzCut(){
  translate([sideThickness,2*lipThickness,pcbDim-7-21])
    rotate([-90,0,0])
      linear_extrude(upperLipThickness)
        polygon(buzzPts);
}
//buzzCut();

module tabs2(withHole=false){
      translate([0,0,pcbDim])
        rotate([-90,0,0])
          tab(withHole);

      mirror([0,0,1])
        rotate([-90,0,0])
          tab(withHole);
  }
//tabs2(true);
  
 module FrontPanel(){
 rotate([0,-90,0]) 
   union(){
    difference(){
      fp();
      union(){
        tabs2(false);
        translate([0,2*lipThickness,0])
          tabs2(false);
        buzzCut();
      }      
    }
    translate([0,2*lipThickness,0])
      tabs2(true);     
  }
}

FrontPanel();

module backPanel(){
  difference(){
    union(){
      bpExtension();
      rotate([0,90,0])
        linear_extrude(pcbDim)
          import(dxfFileName,layer=bpLayer);
    }
    union(){
      yokisStrapBackHole();
      plugsHole();
      btHoles();
      bpTrimmer();
      bpAngleholes();
      bpFootTrimmers();
    }
  }
}
//backPanel();

module bpExtension(){
  translate([-sideThickness,0,0])
    rotate([0,90,0])
      linear_extrude(pcbDim + 2*sideThickness)
        import(dxfFileName,layer=bpExtensionLayer);
}

bpTrimXDim    =  60;
bpTrimXOffset = 6;
module bpTrimmer(){
  translate([bpTrimXOffset,0,0])
    rotate([0,90,0])
      linear_extrude(bpTrimXDim)
        import(dxfFileName,layer=bpTrimLayer);
}
//%bpTrimmer();

yokisStrapBackHolePostion = [pcbDim-6,54,0];
module yokisStrapBackHole(){
    linear_extrude(sideThickness)
      translate(yokisStrapBackHolePostion)
        circle(d=m3d);
}
//yokisStrapBackHole();

plugsHolePostion = [12,0,0];
plugsHoleDim = [58,17];
module plugsHole(){
  linear_extrude(sideThickness+6)
    translate(plugsHolePostion)
      square(plugsHoleDim);
}
//plugsHole();

module btHoles(){
  btHole1();
  translate([pcbDim-2*mountingHoleOffset,0,0])
    btHole1();
}

module btHole1(){
  translate([0,lipThickness+pcbThickness,mountingHoleOffset+sideThickness])
    rotate([-90,0,0])
      linear_extrude(upperLipThickness)
        translate([mountingHoleOffset,0,0])
          circle(d=m3d);
}

//btHoles();

module bpFootTrimmer1(x){
  translate([x,0,0])
    rotate([0,90,0])
      linear_extrude(mountingHoleOffset*2)
          import(dxfFileName,layer=bpFootTrimmerLayer);
}
module bpFootTrimmers(){
  bpFootTrimmer1(0); //mountingHoleOffset);
  bpFootTrimmer1(pcbDim-mountingHoleOffset*2);
}
//bpFootTrimmers();

bpAngleHolePosition = [0,69.35,7.25];
bpAngleHoleLength   = 20;
module bpAngleHole(x){
  translate([x,0,0])
  translate(bpAngleHolePosition )
    rotate([-60,0,0])
      linear_extrude(bpAngleHoleLength )
        //translate([mountingHoleOffset,0,0])
          circle(d=m3d);
}
module bpAngleholes(){
  bpAngleHole(mountingHoleOffset);
  bpAngleHole(pcbDim-mountingHoleOffset);
}

//// yokis strap ////
yokisStrapHeight = 10;
yokisHolePosition = [6,-sideThickness,yokisStrapHeight/2.];

module yokisStrap(){
  difference(){
    linear_extrude(yokisStrapHeight )
      import(dxfStrapFilename,layer = yokiStrapLayer);
    yokisStrapHole();
  }
}
//yokisStrap();
module yokisStrapHole(){
  translate(yokisHolePosition)
    rotate([-90,0,0])
      linear_extrude(sideThickness)
        circle(d=m3d);
}
//%yokisStrapHole();