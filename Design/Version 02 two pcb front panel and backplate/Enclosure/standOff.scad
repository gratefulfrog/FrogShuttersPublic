$fn=100;

dxfFileName = "DXF/Enclosure_side_position.dxf";

spLayer   = "SidePanel";


m2d  = 2.25;
m3d  = 3.4;
m4d  = 4.5;
m5d  = 5.5;
m6d  = 6.5;
m7d  = 7.5;
m8d  = 8.3;
m9d  = 9.3;
m10d = 10.3;

m2w  = 4;
m3w  = 5.5;
m4w  = 4.5;
m5w  = 7;
m6w  = 8;
m7w  = 10;
m8w  = 13;
m10w = 16;


height  = 10;
dia     = m3w/cos(30);
holeDia = m3d;

echo(dia);
echo(m3w/cos(30));

module standOff(){
  difference(){
    cylinder(h=height,d=dia);
    cylinder(h=height,d=holeDia);
  }
}

//standOff();

/// Snap on
wallThickness = 3;
bigDia        = dia+2*wallThickness;
epsilon       = 0.1; // version 02 epsilon changed from 0 to 0.1
smallDia      = dia - epsilon;
alpha         = 25;

sideThickness = 3;

module snapOn(){
  x = bigDia;
  y = bigDia/2.;
  z = height;
  yt = bigDia*cos(30)/2.;
  xp = 4*x*cos(alpha);
  yp = 4*x*sin(alpha);
  pts=[[0,0],[xp,yp],[-xp,yp]];
  //rotate([90,,0]){
    difference(){
      union(){
        translate([0,-(yt-epsilon)/2.,z/2.])
          cube([2*y,yt-epsilon,z],center= true);
        cylinder(d=bigDia,h=z);
      }
      union(){
        cylinder(d=smallDia,h=z); // from version 02 ,$fn=6);
        triangleCutter(pts,z);
        translate([-10,-20-smallDia*cos(30)/2.,-2])
          cube([20,20,20]);  
      }
    }
    translate([0,-smallDia*cos(30)/2.,0])
      sidePanel(); //x,sideThickness,z);
  }
//}
snapOn();

module sidePanel(){
  rotate([90,0,0])
    linear_extrude(sideThickness)
      import(dxfFileName,layer=spLayer);
}
//sidePanel();

tPts=[[0,0],
      [4*bigDia*cos(alpha),4*bigDia*sin(alpha)],
      [-4*bigDia*cos(alpha),4*bigDia*sin(alpha)]];
  
module triangleCutter(pts,h){
  linear_extrude(h)
    polygon(pts);
}
//triangleCutter(tPts,height);
/*
module backPanel(x,y,z){
  xx = 2*x;
  zz = y; //sideThickness;
  topZ = 4.5; // version 03: 5; // version 02: 4;
  bottomZ = 10;
  yy = z + topZ  + bottomZ ;
  //translate([-xx/2.0,-(y/2.+yy),-bottomZ])
    translate([-xx/2.0,-sideThickness,yy-bottomZ])
      rotate([-90,,0])
        //cube([xx,yy,zz],center=false);
        roundedCube([xx,yy,zz],center=false);
}
//backPanel(10,5,3);
*/
module roundedCube(tuple,rr=1,roundZ=false,center=false){
  x = (tuple[0])/2.-rr;
  y = (tuple[1])/2.-rr;
  z = tuple[2]-(roundZ ? rr : 0);
  tp = center ? [0,0,0] : [x+rr,y+rr,  (roundZ ?  rr :  0)];
  translate(tp)
  hull()
  for (p=[[x,y,0],[-x,y,0],[-x,-y,0],[x,-y,0]])
    translate(p)
      if (roundZ)
        sphere(r=rr);
      else
        cylinder(h=z,r=rr,center=center);
      
} 
//roundedCube([30,3,14],roundZ=true,center=true);