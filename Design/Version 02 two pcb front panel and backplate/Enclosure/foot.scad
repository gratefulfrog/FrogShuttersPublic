$fn=100;
m2d  = 2.25;
m3d  = 3.4;
m4d  = 4.5;
m5d  = 5.5;
m6d  = 6.5;
m7d  = 7.5;
m8d  = 8.3;
m9d  = 9.3;
m10d = 10.3;

m2w  = 4;
m3w  = 5.5;
m4w  = 4.5;
m5w  = 7;
m6w  = 8;
m7w  = 10;
m8w  = 13;
m10w = 16;


height     = 10;
topDia     = 6;
bottomDia  = 10;
headDia    = 5.8;
headHeight = 4;
lipHeight  = 4;
baseHeight = 3;

screwDia = m3d;

module screw(length, headDip=0){
  translate([0,0,headHeight])
    cylinder(d=screwDia,h=length);
  translate([0,0,headDip])
    cylinder(d=headDia,h=headHeight-headDip);
}

//screw(20,-5);

module socket(){
  h=height-lipHeight;
  translate([0,0,h])
    cylinder(d=topDia,h=lipHeight);
  translate([0,0,baseHeight])
    cylinder(h=h-baseHeight,d1=bottomDia,d2=topDia);
  cylinder(h=baseHeight,d=bottomDia);
}
//socket();

module foot(){
  difference(){
    socket();
    screw(20,-5);
  }
}
foot();