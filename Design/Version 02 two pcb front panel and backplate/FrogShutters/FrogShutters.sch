EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title "Frog Shutters"
Date "2021-03-02"
Rev "2.2.1"
Comp "gratefulfrog.org"
Comment1 "Version with 2 PCBs"
Comment2 "update SPDT"
Comment3 "update for LCD connector"
Comment4 "Update Keyed Rotary Encoders"
$EndDescr
$Comp
L Device:Buzzer BZ1
U 1 1 5FD514A1
P 3880 5790
F 0 "BZ1" H 4034 5820 50  0000 L CNN
F 1 "Piezo Buzzer" H 4034 5727 50  0000 L CNN
F 2 "misc:Piezo_AT3040" V 3855 5890 50  0001 C CNN
F 3 "~" V 3855 5890 50  0001 C CNN
	1    3880 5790
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5FD58B4F
P 4220 3510
F 0 "R6" V 4210 3610 50  0000 C CNN
F 1 "1K" V 4220 3510 50  0000 C CNN
F 2 "misc:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal_BIG_PADS" V 4150 3510 50  0001 C CNN
F 3 "~" H 4220 3510 50  0001 C CNN
	1    4220 3510
	0    1    1    0   
$EndComp
$Comp
L LH1540AT:LH1540AT U3
U 1 1 5FD5B549
P 9050 3740
F 0 "U3" H 9050 4413 50  0000 C CNN
F 1 "Relay OPEN" H 9050 4320 50  0000 C CNN
F 2 "misc:DIP6_big_pads" H 9050 3740 50  0001 L BNN
F 3 "" H 9050 3740 50  0001 L BNN
F 4 "Vishay" H 9050 3740 50  0001 L BNN "MANUFACTURER"
	1    9050 3740
	1    0    0    -1  
$EndComp
$Comp
L LH1540AT:LH1540AT U4
U 1 1 5FD5D247
P 9060 2330
F 0 "U4" H 9060 3003 50  0000 C CNN
F 1 "Relay CLOSE" H 9060 2910 50  0000 C CNN
F 2 "misc:DIP6_big_pads" H 9060 2330 50  0001 L BNN
F 3 "" H 9060 2330 50  0001 L BNN
F 4 "Vishay" H 9060 2330 50  0001 L BNN "MANUFACTURER"
	1    9060 2330
	1    0    0    -1  
$EndComp
NoConn ~ 9660 2330
NoConn ~ 9650 3740
$Comp
L power:GND #PWR021
U 1 1 5FD8D88D
P 5770 3810
F 0 "#PWR021" H 5770 3560 50  0001 C CNN
F 1 "GND" H 5775 3634 50  0000 C CNN
F 2 "" H 5770 3810 50  0001 C CNN
F 3 "" H 5770 3810 50  0001 C CNN
	1    5770 3810
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR033
U 1 1 5FD8ED5C
P 8460 2330
F 0 "#PWR033" H 8460 2080 50  0001 C CNN
F 1 "GND" H 8465 2154 50  0000 C CNN
F 2 "" H 8460 2330 50  0001 C CNN
F 3 "" H 8460 2330 50  0001 C CNN
	1    8460 2330
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR032
U 1 1 5FD945C3
P 8450 3740
F 0 "#PWR032" H 8450 3490 50  0001 C CNN
F 1 "GND" H 8455 3564 50  0000 C CNN
F 2 "" H 8450 3740 50  0001 C CNN
F 3 "" H 8450 3740 50  0001 C CNN
	1    8450 3740
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5FD96675
P -3320 2850
F 0 "#PWR08" H -3320 2600 50  0001 C CNN
F 1 "GND" V -3315 2674 50  0000 C CNN
F 2 "" H -3320 2850 50  0001 C CNN
F 3 "" H -3320 2850 50  0001 C CNN
	1    -3320 2850
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5FD97CAC
P -1880 2850
F 0 "#PWR013" H -1880 2600 50  0001 C CNN
F 1 "GND" V -1875 2674 50  0000 C CNN
F 2 "" H -1880 2850 50  0001 C CNN
F 3 "" H -1880 2850 50  0001 C CNN
	1    -1880 2850
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5FD98E40
P -4430 4670
F 0 "#PWR04" H -4430 4420 50  0001 C CNN
F 1 "GND" V -4425 4540 50  0000 R CNN
F 2 "" H -4430 4670 50  0001 C CNN
F 3 "" H -4430 4670 50  0001 C CNN
	1    -4430 4670
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR027
U 1 1 5FD9C78E
P -1630 5290
F 0 "#PWR027" H -1630 5040 50  0001 C CNN
F 1 "GND" V -1730 5230 50  0000 C CNN
F 2 "" H -1630 5290 50  0001 C CNN
F 3 "" H -1630 5290 50  0001 C CNN
	1    -1630 5290
	0    1    1    0   
$EndComp
Text Label 5770 3110 0    50   ~ 0
SCL
Text Label 7560 2180 0    50   ~ 0
SCL
Text Label -1630 5590 2    50   ~ 0
SCL_front_panel
Text Label 7560 2080 0    50   ~ 0
SDA
Text Label -1630 5490 2    50   ~ 0
SDA_front_panel
$Comp
L Memory_EEPROM:24LC512 U2
U 1 1 5FD53116
P 7160 2180
F 0 "U2" H 7410 1750 50  0000 C CNN
F 1 "EEPROM" H 7570 1900 50  0000 C CNN
F 2 "misc:DIP-8_W7.62mm_big_pads" H 7160 2180 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21754M.pdf" H 7160 2180 50  0001 C CNN
	1    7160 2180
	1    0    0    -1  
$EndComp
Text Label 5770 3010 0    50   ~ 0
SDA
Text Label -3320 3000 0    50   ~ 0
SigLeft_front_panel
Text Label -1880 3000 0    50   ~ 0
SigRight_front_panel
Text Label -1880 3100 0    50   ~ 0
DirRight_front_panel
Text Label -3320 3100 0    50   ~ 0
DirLeft_front_panel
Text Label -3320 3200 0    50   ~ 0
KeyLeft_front_panel
$Comp
L power:GND #PWR029
U 1 1 5FE5BF0E
P 7160 2480
F 0 "#PWR029" H 7160 2230 50  0001 C CNN
F 1 "GND" H 7165 2304 50  0000 C CNN
F 2 "" H 7160 2480 50  0001 C CNN
F 3 "" H 7160 2480 50  0001 C CNN
	1    7160 2480
	1    0    0    -1  
$EndComp
Text Label 8450 3340 2    50   ~ 0
YokisOpenPin
Text Label 8460 1930 2    50   ~ 0
YokisClosePin
Text Label 9660 2730 0    50   ~ 0
Yokis_Transmitter_Common_(White)
Text Label 9650 4140 0    50   ~ 0
Yokis_Transmitter_Common_(White)
Text Label 9650 3340 0    50   ~ 0
Yokis_Transmitter_Open_(Green)
Text Label 9660 1930 0    50   ~ 0
Yokis_Transmitter_Close_(Brown)
$Comp
L Device:C C4
U 1 1 5FE718BD
P 7310 1880
F 0 "C4" V 7055 1880 50  0000 C CNN
F 1 "100 nF" V 7148 1880 50  0000 C CNN
F 2 "misc:C_Disc_D4.7mm_W2.5mm_P5.00mm_big_pads" H 7348 1730 50  0001 C CNN
F 3 "~" H 7310 1880 50  0001 C CNN
	1    7310 1880
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR030
U 1 1 5FE725D0
P 7460 1880
F 0 "#PWR030" H 7460 1630 50  0001 C CNN
F 1 "GND" V 7465 1750 50  0000 R CNN
F 2 "" H 7460 1880 50  0001 C CNN
F 3 "" H 7460 1880 50  0001 C CNN
	1    7460 1880
	0    -1   -1   0   
$EndComp
Text Label 5770 2710 0    50   ~ 0
KeyLeft_no_pull
Text Label 5770 2510 0    50   ~ 0
SigLeft_no_pull
Text Label 5770 2610 0    50   ~ 0
DirLeft_no_pull
Text Label 5770 1910 0    50   ~ 0
KeyRight_no_pull
Text Label 5770 2310 0    50   ~ 0
SigRight_no_pull
Text Label 5770 2010 0    50   ~ 0
DirRight_no_pull
Text Label 4370 3310 2    50   ~ 0
WindSensor_no_pull
Text Label 4070 3510 2    50   ~ 0
BuzzerPin
Text Label 4070 3410 2    50   ~ 0
YokisOpenPin
Text Label 4070 3210 2    50   ~ 0
YokisClosePin
NoConn ~ 4370 1910
NoConn ~ 4370 2610
NoConn ~ 4370 2910
NoConn ~ 5770 3310
NoConn ~ 5770 3210
NoConn ~ 5770 2910
$Comp
L Connector:Screw_Terminal_01x03 J5
U 1 1 5FD865D7
P 10020 4590
F 0 "J5" H 10100 4632 50  0000 L CNN
F 1 "Yokis" H 9690 4370 50  0000 L CNN
F 2 "misc:2EDG_1x03_P5.00mm_Horizontal_pads_ok" H 10020 4590 50  0001 C CNN
F 3 "~" H 10020 4590 50  0001 C CNN
	1    10020 4590
	1    0    0    -1  
$EndComp
Text Label 9820 4490 2    50   ~ 0
Yokis_Transmitter_Common_(White)
Text Label 9820 4590 2    50   ~ 0
Yokis_Transmitter_Open_(Green)
Text Label 9820 4690 2    50   ~ 0
Yokis_Transmitter_Close_(Brown)
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5FD9A2CA
P 4370 2310
F 0 "#FLG01" H 4370 2385 50  0001 C CNN
F 1 "PWR_FLAG" H 4270 2250 50  0000 C CNN
F 2 "" H 4370 2310 50  0001 C CNN
F 3 "~" H 4370 2310 50  0001 C CNN
	1    4370 2310
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J3
U 1 1 5FDA115B
P 3030 4830
F 0 "J3" H 3110 4822 50  0000 L CNN
F 1 "Wind Sensor" H 2800 4970 50  0000 L CNN
F 2 "misc:2EDG_1x02_P5.00mm_Horizontal_pads_ok" H 3030 4830 50  0001 C CNN
F 3 "~" H 3030 4830 50  0001 C CNN
	1    3030 4830
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR019
U 1 1 5FDA3B04
P 2830 4830
F 0 "#PWR019" H 2830 4580 50  0001 C CNN
F 1 "GND" H 2835 4654 50  0000 C CNN
F 2 "" H 2830 4830 50  0001 C CNN
F 3 "" H 2830 4830 50  0001 C CNN
	1    2830 4830
	-1   0    0    1   
$EndComp
Text Label 2620 4930 2    50   ~ 0
WindSensor_no_pull
$Comp
L Device:R R5
U 1 1 5FD7C8C4
P 4220 3410
F 0 "R5" V 4260 3250 50  0000 L CNN
F 1 "390R" V 4210 3310 50  0000 L CNN
F 2 "misc:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal_BIG_PADS" V 4150 3410 50  0001 C CNN
F 3 "~" H 4220 3410 50  0001 C CNN
	1    4220 3410
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR025
U 1 1 5FE33C1B
P -1910 5090
F 0 "#PWR025" H -1910 4840 50  0001 C CNN
F 1 "GND" V -2020 5090 50  0000 C CNN
F 2 "" H -1910 5090 50  0001 C CNN
F 3 "" H -1910 5090 50  0001 C CNN
	1    -1910 5090
	-1   0    0    1   
$EndComp
$Comp
L Device:R R7
U 1 1 5FEBAD96
P 4220 3810
F 0 "R7" V 4260 3900 50  0000 C CNN
F 1 "1K" V 4220 3810 50  0000 C CNN
F 2 "misc:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal_BIG_PADS" V 4150 3810 50  0001 C CNN
F 3 "~" H 4220 3810 50  0001 C CNN
	1    4220 3810
	0    1    1    0   
$EndComp
$Comp
L Device:R R8
U 1 1 5FEE6974
P 5920 2110
F 0 "R8" V 5960 2210 50  0000 C CNN
F 1 "1K" V 5920 2110 50  0000 C CNN
F 2 "misc:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal_BIG_PADS" V 5850 2110 50  0001 C CNN
F 3 "~" H 5920 2110 50  0001 C CNN
	1    5920 2110
	0    -1   -1   0   
$EndComp
Text Label 6070 2110 0    50   ~ 0
LED_CLOSE
Text Label -4130 4670 0    50   ~ 0
LED_OPEN_front_panel
Text Label -4120 4240 0    50   ~ 0
LED_CLOSE_front_panel
Text Label -4110 3820 0    50   ~ 0
LED_PULSE_front_panel
$Comp
L Switch:SW_Coded SW3
U 1 1 5FF38059
P -2380 3100
F 0 "SW3" H -2247 3580 50  0000 C CNN
F 1 "Rotary_Switch_Right" H -2247 3487 50  0000 C CNN
F 2 "misc:Keyed_Rotary_Encoder" H -2405 3125 50  0001 C CNN
F 3 "~" H -2405 3125 50  0001 C CNN
	1    -2380 3100
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H8
U 1 1 5FD6FD1E
P 3020 6780
F 0 "H8" H 3120 6827 50  0001 L CNN
F 1 "MountingHole" H 3120 6734 50  0001 L CNN
F 2 "misc:MountingHole_3.2mm_M3_nut_courtyard" H 3020 6780 50  0001 C CNN
F 3 "~" H 3020 6780 50  0001 C CNN
	1    3020 6780
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H7
U 1 1 5FD72FB4
P 3010 6580
F 0 "H7" H 3110 6627 50  0001 L CNN
F 1 "MountingHole" H 3110 6534 50  0001 L CNN
F 2 "misc:MountingHole_3.2mm_M3_nut_courtyard" H 3010 6580 50  0001 C CNN
F 3 "~" H 3010 6580 50  0001 C CNN
	1    3010 6580
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H9
U 1 1 5FD73839
P 3210 6580
F 0 "H9" H 3310 6627 50  0001 L CNN
F 1 "MountingHole" H 3310 6534 50  0001 L CNN
F 2 "misc:MountingHole_3.2mm_M3_nut_courtyard" H 3210 6580 50  0001 C CNN
F 3 "~" H 3210 6580 50  0001 C CNN
	1    3210 6580
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H10
U 1 1 5FD7A8CB
P 3210 6780
F 0 "H10" H 3310 6827 50  0001 L CNN
F 1 "MountingHole" H 3310 6734 50  0001 L CNN
F 2 "misc:MountingHole_3.2mm_M3_nut_courtyard" H 3210 6780 50  0001 C CNN
F 3 "~" H 3210 6780 50  0001 C CNN
	1    3210 6780
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR024
U 1 1 5FE5E874
P 6760 2280
F 0 "#PWR024" H 6760 2030 50  0001 C CNN
F 1 "GND" V 6765 2150 50  0000 R CNN
F 2 "" H 6760 2280 50  0001 C CNN
F 3 "" H 6760 2280 50  0001 C CNN
	1    6760 2280
	1    0    0    -1  
$EndComp
Text Notes 2930 6480 0    50   ~ 0
Rear PCB\nMounting\nHoles
Wire Wire Line
	6760 2070 6760 2080
Connection ~ 6760 2080
Wire Wire Line
	6760 2080 6760 2180
Connection ~ 6760 2180
Wire Wire Line
	6760 2180 6760 2280
Connection ~ 6760 2280
$Comp
L Connector:Conn_01x04_Male J4
U 1 1 5FDBC981
P -1430 5390
F 0 "J4" H -1458 5271 50  0000 R CNN
F 1 "LCD" H -1440 5360 50  0000 R CNN
F 2 "misc:LCD_with_I2C_Backpack" H -1430 5390 50  0001 C CNN
F 3 "~" H -1430 5390 50  0001 C CNN
	1    -1430 5390
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5FF7747A
P 4220 3210
F 0 "R4" V 4260 3050 50  0000 L CNN
F 1 "390R" V 4210 3110 50  0000 L CNN
F 2 "misc:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal_BIG_PADS" V 4150 3210 50  0001 C CNN
F 3 "~" H 4220 3210 50  0001 C CNN
	1    4220 3210
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J6
U 1 1 5FF85EA4
P 10060 5270
F 0 "J6" H 10140 5262 50  0000 L CNN
F 1 "Buzzer" H 9830 5410 50  0000 L CNN
F 2 "TerminalBlock_TE-Connectivity:TerminalBlock_TE_282834-2_1x02_P2.54mm_Horizontal" H 10060 5270 50  0001 C CNN
F 3 "~" H 10060 5270 50  0001 C CNN
	1    10060 5270
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR034
U 1 1 5FF8821A
P 9860 5370
F 0 "#PWR034" H 9860 5120 50  0001 C CNN
F 1 "GND" H 9865 5194 50  0000 C CNN
F 2 "" H 9860 5370 50  0001 C CNN
F 3 "" H 9860 5370 50  0001 C CNN
	1    9860 5370
	0    1    1    0   
$EndComp
Text Label 9860 5270 2    50   ~ 0
BuzzerPin
NoConn ~ 3780 5690
NoConn ~ 3780 5890
$Comp
L power:GNDREF #PWR022
U 1 1 60000918
P 5770 3810
F 0 "#PWR022" H 5770 3560 50  0001 C CNN
F 1 "GNDREF" V 5775 3680 50  0000 R CNN
F 2 "" H 5770 3810 50  0001 C CNN
F 3 "" H 5770 3810 50  0001 C CNN
	1    5770 3810
	0    -1   -1   0   
$EndComp
Connection ~ 5770 3810
$Comp
L power:GNDPWR #PWR023
U 1 1 60002FA8
P 5770 3810
F 0 "#PWR023" H 5770 3610 50  0001 C CNN
F 1 "GNDPWR" H 5776 3654 50  0000 C CNN
F 2 "" H 5770 3760 50  0001 C CNN
F 3 "" H 5770 3760 50  0001 C CNN
	1    5770 3810
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR031
U 1 1 601E7C30
P 7560 2280
F 0 "#PWR031" H 7560 2030 50  0001 C CNN
F 1 "GND" V 7565 2150 50  0000 R CNN
F 2 "" H 7560 2280 50  0001 C CNN
F 3 "" H 7560 2280 50  0001 C CNN
	1    7560 2280
	0    -1   -1   0   
$EndComp
NoConn ~ 4370 2110
$Comp
L power:+3.3V #PWR020
U 1 1 6005A3FF
P 4370 2310
F 0 "#PWR020" H 4370 2160 50  0001 C CNN
F 1 "+3.3V" V 4385 2440 50  0000 L CNN
F 2 "" H 4370 2310 50  0001 C CNN
F 3 "" H 4370 2310 50  0001 C CNN
	1    4370 2310
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR028
U 1 1 6005C960
P 7160 1880
F 0 "#PWR028" H 7160 1730 50  0001 C CNN
F 1 "+3.3V" V 7175 2010 50  0000 L CNN
F 2 "" H 7160 1880 50  0001 C CNN
F 3 "" H 7160 1880 50  0001 C CNN
	1    7160 1880
	0    -1   -1   0   
$EndComp
Connection ~ 7160 1880
$Comp
L power:+3.3V #PWR026
U 1 1 60061962
P -1910 5390
F 0 "#PWR026" H -1910 5240 50  0001 C CNN
F 1 "+3.3V" V -1895 5520 50  0000 L CNN
F 2 "" H -1910 5390 50  0001 C CNN
F 3 "" H -1910 5390 50  0001 C CNN
	1    -1910 5390
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C3
U 1 1 5FDAABFD
P -1910 5240
F 0 "C3" V -1710 5240 50  0000 C CNN
F 1 "10µF" V -1780 5240 50  0000 C CNN
F 2 "misc:CP_Radial_D5.0mm_P2.50mm_big_pads" H -1872 5090 50  0001 C CNN
F 3 "~" H -1910 5240 50  0001 C CNN
	1    -1910 5240
	-1   0    0    1   
$EndComp
Wire Wire Line
	-1630 5390 -1910 5390
Connection ~ -1910 5390
$Comp
L power:+3.3V #PWR07
U 1 1 6006DCB9
P -3320 3300
F 0 "#PWR07" H -3320 3150 50  0001 C CNN
F 1 "+3.3V" V -3305 3430 50  0000 L CNN
F 2 "" H -3320 3300 50  0001 C CNN
F 3 "" H -3320 3300 50  0001 C CNN
	1    -3320 3300
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR017
U 1 1 601C6ABC
P 2620 4560
F 0 "#PWR017" H 2620 4410 50  0001 C CNN
F 1 "+3.3V" V 2635 4690 50  0000 L CNN
F 2 "" H 2620 4560 50  0001 C CNN
F 3 "" H 2620 4560 50  0001 C CNN
	1    2620 4560
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 601C9BEF
P 2620 4710
F 0 "R3" H 2710 4710 50  0000 C CNN
F 1 "4K7" V 2620 4710 50  0000 C CNN
F 2 "misc:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal_BIG_PADS" V 2550 4710 50  0001 C CNN
F 3 "~" H 2620 4710 50  0001 C CNN
	1    2620 4710
	-1   0    0    1   
$EndComp
$Comp
L Device:C C2
U 1 1 601CA678
P 2620 5080
F 0 "C2" H 2820 5080 50  0000 C CNN
F 1 "15nF" H 2750 5210 50  0000 C CNN
F 2 "misc:C_Disc_D4.7mm_W2.5mm_P5.00mm_big_pads" H 2658 4930 50  0001 C CNN
F 3 "~" H 2620 5080 50  0001 C CNN
	1    2620 5080
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR018
U 1 1 601DC90D
P 2620 5230
F 0 "#PWR018" H 2620 4980 50  0001 C CNN
F 1 "GND" H 2625 5054 50  0000 C CNN
F 2 "" H 2620 5230 50  0001 C CNN
F 3 "" H 2620 5230 50  0001 C CNN
	1    2620 5230
	1    0    0    -1  
$EndComp
Wire Wire Line
	2830 4930 2620 4930
Wire Wire Line
	2620 4930 2620 4860
Connection ~ 2620 4930
Text Label -1880 3200 0    50   ~ 0
KeyRight_front_panel
$Comp
L power:+3.3V #PWR012
U 1 1 6006D54D
P -1880 3300
F 0 "#PWR012" H -1880 3150 50  0001 C CNN
F 1 "+3.3V" V -1865 3430 50  0000 L CNN
F 2 "" H -1880 3300 50  0001 C CNN
F 3 "" H -1880 3300 50  0001 C CNN
	1    -1880 3300
	0    1    1    0   
$EndComp
$Comp
L led:LED5MM LD3
U 1 1 6020DCBB
P -4210 3820
F 0 "LD3" V -4515 3770 50  0000 C CNN
F 1 "LED_BLUE_5MM" V -4422 3770 50  0000 C CNN
F 2 "misc:LED_D5.0mm_big_pads" H -4210 3970 50  0001 C CNN
F 3 "" H -4210 3820 50  0001 C CNN
	1    -4210 3820
	0    1    1    0   
$EndComp
$Comp
L led:LED5MM LD2
U 1 1 6020E656
P -4220 4240
F 0 "LD2" V -4525 4190 50  0000 C CNN
F 1 "LED_RED_5MM" V -4432 4190 50  0000 C CNN
F 2 "misc:LED_D5.0mm_big_pads" H -4220 4390 50  0001 C CNN
F 3 "" H -4220 4240 50  0001 C CNN
	1    -4220 4240
	0    1    1    0   
$EndComp
$Comp
L led:LED5MM LD1
U 1 1 6020EE86
P -4230 4670
F 0 "LD1" V -4535 4620 50  0000 C CNN
F 1 "LED_GREEN_5MM" V -4442 4620 50  0000 C CNN
F 2 "misc:LED_D5.0mm_big_pads" H -4230 4820 50  0001 C CNN
F 3 "" H -4230 4670 50  0001 C CNN
	1    -4230 4670
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR05
U 1 1 60214510
P -4420 4240
F 0 "#PWR05" H -4420 3990 50  0001 C CNN
F 1 "GND" V -4415 4110 50  0000 R CNN
F 2 "" H -4420 4240 50  0001 C CNN
F 3 "" H -4420 4240 50  0001 C CNN
	1    -4420 4240
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR06
U 1 1 60214C07
P -4410 3820
F 0 "#PWR06" H -4410 3570 50  0001 C CNN
F 1 "GND" V -4405 3690 50  0000 R CNN
F 2 "" H -4410 3820 50  0001 C CNN
F 3 "" H -4410 3820 50  0001 C CNN
	1    -4410 3820
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 6024FF49
P -4280 5330
F 0 "R1" V -4190 5330 50  0000 C CNN
F 1 "4K7" V -4280 5330 50  0000 C CNN
F 2 "misc:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal_BIG_PADS" V -4350 5330 50  0001 C CNN
F 3 "~" H -4280 5330 50  0001 C CNN
	1    -4280 5330
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R2
U 1 1 602510B1
P -4280 5530
F 0 "R2" V -4190 5530 50  0000 C CNN
F 1 "4K7" V -4280 5530 50  0000 C CNN
F 2 "misc:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal_BIG_PADS" V -4350 5530 50  0001 C CNN
F 3 "~" H -4280 5530 50  0001 C CNN
	1    -4280 5530
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 60251727
P -4830 5430
F 0 "#PWR01" H -4830 5180 50  0001 C CNN
F 1 "GND" H -4825 5254 50  0000 C CNN
F 2 "" H -4830 5430 50  0001 C CNN
F 3 "" H -4830 5430 50  0001 C CNN
	1    -4830 5430
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR02
U 1 1 6027992D
P -4130 5330
F 0 "#PWR02" H -4130 5180 50  0001 C CNN
F 1 "+3.3V" V -4190 5310 50  0000 L CNN
F 2 "" H -4130 5330 50  0001 C CNN
F 3 "" H -4130 5330 50  0001 C CNN
	1    -4130 5330
	0    1    1    0   
$EndComp
Wire Wire Line
	-4130 5330 -4130 5530
Connection ~ -4130 5330
Wire Wire Line
	-3820 5160 -4430 5160
Wire Wire Line
	-4430 5160 -4430 5330
Wire Wire Line
	-4430 5530 -4430 5940
Wire Wire Line
	-4430 5940 -3820 5940
Wire Wire Line
	-3820 5360 -3820 5450
Wire Wire Line
	-3820 5450 -3220 5450
Wire Wire Line
	-3220 5450 -3220 5840
Wire Wire Line
	-3820 5570 -3140 5570
Wire Wire Line
	-3140 5570 -3140 5260
Wire Wire Line
	-3140 5260 -3220 5260
Connection ~ -3140 5260
Wire Wire Line
	-3140 5260 -3070 5260
Text Label -3070 5260 0    50   ~ 0
NAND_Gate_front_panel
$Comp
L power:+3.3V #PWR09
U 1 1 60327F6F
P -3020 6280
F 0 "#PWR09" H -3020 6130 50  0001 C CNN
F 1 "+3.3V" V -3080 6260 50  0000 L CNN
F 2 "" H -3020 6280 50  0001 C CNN
F 3 "" H -3020 6280 50  0001 C CNN
	1    -3020 6280
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR03
U 1 1 603296C6
P -4020 6280
F 0 "#PWR03" H -4020 6030 50  0001 C CNN
F 1 "GND" H -4015 6104 50  0000 C CNN
F 2 "" H -4020 6280 50  0001 C CNN
F 3 "" H -4020 6280 50  0001 C CNN
	1    -4020 6280
	0    1    1    0   
$EndComp
NoConn ~ -2890 5760
NoConn ~ -2890 5560
NoConn ~ -2890 5880
NoConn ~ -2890 6080
NoConn ~ -2290 5980
NoConn ~ -2290 5660
Wire Wire Line
	-3820 5570 -3820 5740
$Comp
L Mechanical:MountingHole H1
U 1 1 6035E49A
P -2200 6410
F 0 "H1" H -2100 6457 50  0001 L CNN
F 1 "MountingHole" H -2100 6364 50  0001 L CNN
F 2 "misc:MountingHole_3.2mm_M3_nut_courtyard" H -2200 6410 50  0001 C CNN
F 3 "~" H -2200 6410 50  0001 C CNN
	1    -2200 6410
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 6035FEB5
P -2040 6410
F 0 "H3" H -1940 6457 50  0001 L CNN
F 1 "MountingHole" H -1940 6364 50  0001 L CNN
F 2 "misc:MountingHole_3.2mm_M3_nut_courtyard" H -2040 6410 50  0001 C CNN
F 3 "~" H -2040 6410 50  0001 C CNN
	1    -2040 6410
	1    0    0    -1  
$EndComp
Text Notes -1890 6600 0    50   ~ 0
Front PCB\nMounting\nHoles
$Comp
L Switch:SW_Coded SW2
U 1 1 5FF38569
P -3820 3100
F 0 "SW2" H -3687 3580 50  0000 C CNN
F 1 "Rotary_Switch_Left" H -3687 3487 50  0000 C CNN
F 2 "misc:Keyed_Rotary_Encoder" H -3845 3125 50  0001 C CNN
F 3 "~" H -3845 3125 50  0001 C CNN
	1    -3820 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 601CA821
P -3020 6430
F 0 "C1" V -2820 6430 50  0000 C CNN
F 1 "100nF" V -2890 6430 50  0000 C CNN
F 2 "misc:C_Disc_D4.7mm_W2.5mm_P5.00mm_big_pads" H -2982 6280 50  0001 C CNN
F 3 "~" H -3020 6430 50  0001 C CNN
	1    -3020 6430
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR010
U 1 1 601D8565
P -3020 6580
F 0 "#PWR010" H -3020 6330 50  0001 C CNN
F 1 "GND" H -3015 6404 50  0000 C CNN
F 2 "" H -3020 6580 50  0001 C CNN
F 3 "" H -3020 6580 50  0001 C CNN
	1    -3020 6580
	0    -1   -1   0   
$EndComp
$Sheet
S -5820 2020 5530 5250
U 60338964
F0 "Front Panel" 50
F1 "fileFrontPanel.sch" 50
$EndSheet
$Comp
L power:+3.3V #PWR015
U 1 1 6048F53B
P 2370 2590
F 0 "#PWR015" H 2370 2440 50  0001 C CNN
F 1 "+3.3V" V 2385 2720 50  0000 L CNN
F 2 "" H 2370 2590 50  0001 C CNN
F 3 "" H 2370 2590 50  0001 C CNN
	1    2370 2590
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR016
U 1 1 6048F541
P 1870 2590
F 0 "#PWR016" H 1870 2340 50  0001 C CNN
F 1 "GND" V 1875 2414 50  0000 C CNN
F 2 "" H 1870 2590 50  0001 C CNN
F 3 "" H 1870 2590 50  0001 C CNN
	1    1870 2590
	0    1    1    0   
$EndComp
Text Label 2370 2690 0    50   ~ 0
KeyRight_no_pull
Text Label 2370 2790 0    50   ~ 0
DirRight_no_pull
Text Label 2370 2890 0    50   ~ 0
SigRight_no_pull
Text Label 2370 2290 0    50   ~ 0
KeyLeft_no_pull
Text Label 2370 2390 0    50   ~ 0
DirLeft_no_pull
Text Label 2370 2490 0    50   ~ 0
SigLeft_no_pull
Text Label 2370 2990 0    50   ~ 0
NAND_Gate_no_pull
Text Label 1870 2390 2    50   ~ 0
SDA
Text Label 1870 2490 2    50   ~ 0
SCL
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J2
U 1 1 6031A1DD
P 2070 2590
F 0 "J2" H 2120 3110 50  0000 C CNN
F 1 "Conn_02x08_Odd_Even" H 2120 3017 50  0000 C CNN
F 2 "misc:IDC-Header_2x08_P2.54mm_Vertical_big_pads" H 2070 2590 50  0001 C CNN
F 3 "~" H 2070 2590 50  0001 C CNN
	1    2070 2590
	1    0    0    -1  
$EndComp
Text Label 6070 2210 0    50   ~ 0
LED_PULSE
$Comp
L Device:R R9
U 1 1 5FEE5B2C
P 5920 2210
F 0 "R9" V 5870 2110 50  0000 C CNN
F 1 "1K" V 5920 2220 50  0000 C CNN
F 2 "misc:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal_BIG_PADS" V 5850 2210 50  0001 C CNN
F 3 "~" H 5920 2210 50  0001 C CNN
	1    5920 2210
	0    1    1    0   
$EndComp
Connection ~ 4370 2310
$Comp
L eec:MKR_WIFI_1010 MD1
U 1 1 5FD4FAFE
P 4270 1910
F 0 "MD1" H 5070 2180 50  0000 C CNN
F 1 "MKR_WIFI_1010" H 5070 2087 50  0000 C CNN
F 2 "misc:Arduino-MKR_WIFI_1010-0-0-MFG_big_big_pads" H 4270 2310 50  0001 L CNN
F 3 "https://store.arduino.cc/mkr-wifi-1010" H 4270 2410 50  0001 L CNN
F 4 "32KBytes" H 4270 2510 50  0001 L CNN "RAM size"
F 5 "No" H 4270 2610 50  0001 L CNN "automotive"
F 6 "UNK" H 4270 2710 50  0001 L CNN "category"
F 7 "ARM Cortex-M0+" H 4270 2810 50  0001 L CNN "core architecture"
F 8 "32bits" H 4270 2910 50  0001 L CNN "data bus width"
F 9 "Integrated Circuits (ICs)" H 4270 3010 50  0001 L CNN "device class L1"
F 10 "Embedded Processors and Controllers" H 4270 3110 50  0001 L CNN "device class L2"
F 11 "Microcontrollers" H 4270 3210 50  0001 L CNN "device class L3"
F 12 "ARDUINO MKR WIFI 1010" H 4270 3310 50  0001 L CNN "digikey description"
F 13 "1050-1162-ND" H 4270 3410 50  0001 L CNN "digikey part number"
F 14 "48MHz" H 4270 3510 50  0001 L CNN "frequency"
F 15 "I2C,SPI,UART,I2S" H 4270 3610 50  0001 L CNN "interface"
F 16 "Yes" H 4270 3710 50  0001 L CNN "lead free"
F 17 "7eb282d1527b7afb" H 4270 3810 50  0001 L CNN "library id"
F 18 "Arduino" H 4270 3910 50  0001 L CNN "manufacturer"
F 19 "5V" H 4270 4010 50  0001 L CNN "max supply voltage"
F 20 "256KBytes" H 4270 4110 50  0001 L CNN "memory size"
F 21 "FLASH" H 4270 4210 50  0001 L CNN "memory type"
F 22 "3.3V" H 4270 4310 50  0001 L CNN "min supply voltage"
F 23 "782-ABX00023" H 4270 4410 50  0001 L CNN "mouser part number"
F 24 "1" H 4270 4510 50  0001 L CNN "number of A/D converters"
F 25 "7" H 4270 4610 50  0001 L CNN "number of ADC channels"
F 26 "8" H 4270 4710 50  0001 L CNN "number of I/Os"
F 27 "1" H 4270 4810 50  0001 L CNN "number of I2C channels"
F 28 "12" H 4270 4910 50  0001 L CNN "number of PWM channels"
F 29 "1" H 4270 5010 50  0001 L CNN "number of SPI channels"
F 30 "1" H 4270 5110 50  0001 L CNN "number of UART channels"
F 31 "EVALUATION_BOARD" H 4270 5210 50  0001 L CNN "package"
F 32 "Yes" H 4270 5310 50  0001 L CNN "rohs"
	1    4270 1910
	1    0    0    -1  
$EndComp
Text Label 4070 3810 2    50   ~ 0
LED_OPEN
Text Label 5770 2810 0    50   ~ 0
Pin_9_Reserve
Text Label 4370 3710 2    50   ~ 0
Pin_A5_Reserve
NoConn ~ 4370 3610
$Comp
L power:+3.3V #PWR0101
U 1 1 60242EDB
P -1600 4110
F 0 "#PWR0101" H -1600 3960 50  0001 C CNN
F 1 "+3.3V" V -1585 4240 50  0000 L CNN
F 2 "" H -1600 4110 50  0001 C CNN
F 3 "" H -1600 4110 50  0001 C CNN
	1    -1600 4110
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 60242EE1
P -2100 4110
F 0 "#PWR0102" H -2100 3860 50  0001 C CNN
F 1 "GND" V -2095 3934 50  0000 C CNN
F 2 "" H -2100 4110 50  0001 C CNN
F 3 "" H -2100 4110 50  0001 C CNN
	1    -2100 4110
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J1
U 1 1 60242EF5
P -1900 4110
F 0 "J1" H -1850 4630 50  0000 C CNN
F 1 "Conn_02x08_Odd_Even" H -1850 4537 50  0000 C CNN
F 2 "misc:IDC-Header_2x08_P2.54mm_Vertical_big_pads" H -1900 4110 50  0001 C CNN
F 3 "~" H -1900 4110 50  0001 C CNN
	1    -1900 4110
	1    0    0    -1  
$EndComp
Text Label 1870 2290 2    50   ~ 0
Pin_9_Reserve
Text Label 1870 2690 2    50   ~ 0
Pin_A5_Reserve
Text Label 1870 2790 2    50   ~ 0
LED_OPEN
Text Label 1870 2890 2    50   ~ 0
LED_CLOSE
Text Label 1870 2990 2    50   ~ 0
LED_PULSE
Text Label 5770 2410 0    50   ~ 0
NAND_Gate_no_pull
Text Label -1600 4510 0    50   ~ 0
NAND_Gate_front_panel
Text Label -1600 4410 0    50   ~ 0
SigRight_front_panel
Text Label -1600 4310 0    50   ~ 0
DirRight_front_panel
Text Label -1600 4210 0    50   ~ 0
KeyRight_front_panel
Text Label -1600 4010 0    50   ~ 0
SigLeft_front_panel
Text Label -1600 3910 0    50   ~ 0
DirLeft_front_panel
Text Label -1600 3810 0    50   ~ 0
KeyLeft_front_panel
Text Label -2100 3810 2    50   ~ 0
Pin_9_Reserve_front_panel
Text Label -2100 4210 2    50   ~ 0
Pin_A5_Reserve_front_panel
Text Label -2100 4410 2    50   ~ 0
LED_CLOSE_front_panel
Text Label -2100 4510 2    50   ~ 0
LED_PULSE_front_panel
Text Label -2100 4310 2    50   ~ 0
LED_OPEN_front_panel
Text Label -2100 4010 2    50   ~ 0
SCL_front_panel
Text Label -2100 3910 2    50   ~ 0
SDA_front_panel
$Comp
L 4xxx:HEF4093B U1
U 1 1 601E7477
P -3520 5260
F 0 "U1" H -3520 5590 50  0000 C CNN
F 1 "NAND Schmitt" H -3520 5497 50  0000 C CNN
F 2 "misc:DIP-14_W7.62mm_big_pads" H -3520 5260 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/HEF4093B.pdf" H -3520 5260 50  0001 C CNN
	1    -3520 5260
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:HEF4093B U1
U 2 1 601E96F7
P -3520 5840
F 0 "U1" H -3520 6170 50  0000 C CNN
F 1 "NAND Schmitt" H -3520 6077 50  0000 C CNN
F 2 "misc:DIP-14_W7.62mm_big_pads" H -3520 5840 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/HEF4093B.pdf" H -3520 5840 50  0001 C CNN
	2    -3520 5840
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:HEF4093B U1
U 3 1 601EC833
P -2590 5660
F 0 "U1" H -2590 5990 50  0000 C CNN
F 1 "NAND Schmitt" H -2590 5897 50  0000 C CNN
F 2 "misc:DIP-14_W7.62mm_big_pads" H -2590 5660 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/HEF4093B.pdf" H -2590 5660 50  0001 C CNN
	3    -2590 5660
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:HEF4093B U1
U 4 1 601EEBCA
P -2590 5980
F 0 "U1" H -2590 6310 50  0000 C CNN
F 1 "NAND Schmitt" H -2590 6217 50  0000 C CNN
F 2 "misc:DIP-14_W7.62mm_big_pads" H -2590 5980 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/HEF4093B.pdf" H -2590 5980 50  0001 C CNN
	4    -2590 5980
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:HEF4093B U1
U 5 1 601F1715
P -3520 6280
F 0 "U1" V -3890 6280 50  0000 C CNN
F 1 "NAND Schmitt" V -3797 6280 50  0000 C CNN
F 2 "misc:DIP-14_W7.62mm_big_pads" H -3520 6280 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/HEF4093B.pdf" H -3520 6280 50  0001 C CNN
	5    -3520 6280
	0    1    1    0   
$EndComp
Connection ~ -3020 6280
$Comp
L Switch_Bob:SW_SPDT SW1
U 1 1 6024F6A8
P -4630 5430
F 0 "SW1" H -4630 5720 50  0000 C CNN
F 1 "SW_SPDT" H -4630 5627 50  0000 C CNN
F 2 "misc:SPDT_Tayda_P4.7mm" H -4630 5430 50  0001 C CNN
F 3 "~" H -4630 5430 50  0001 C CNN
	1    -4630 5430
	1    0    0    -1  
$EndComp
Connection ~ -4430 5330
Connection ~ -4430 5530
$EndSCHEMATC
